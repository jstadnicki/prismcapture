﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Capture.Common.Repository;

namespace Capture.Module.Repository.EFBasedImplementation
{
    public sealed class RepositoryModule : IModule
    {
        private IUnityContainer container;

        public RepositoryModule(IUnityContainer container)
        {
            this.container = container;
        }

        public void Initialize()
        {
            this.container.RegisterType<IRepository, Repository>(new ContainerControlledLifetimeManager());
        }
    }
}
