﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Capture.Common.Repository;
using Capture.Model;
using System.Data.Objects;
using Capture.Common.Model;

namespace Capture.Module.Repository.EFBasedImplementation
{
    public sealed class Repository : IRepository
    {
        public IEnumerable<ITradeListElemet> GetTradeList(int startIndex, int count, TradeFilter filter)
        {
            IEnumerable<ITradeListElemet> result;
            IEnumerable<Trade> tradeset = null;


            using (var db = new TCEntities())
            {
                if (filter == null)
                {
                    tradeset = this.LoadTrades(db, startIndex, count);
                }
                else
                {
                    tradeset = this.LoadTrades(db, startIndex, count, filter);
                }

                result = this.MakeListOfTradeListElements(tradeset);
            }

            return result;
        }

        private IEnumerable<Trade> LoadTrades(TCEntities db, int startIndex, int count, TradeFilter filter)
        {
            var rst1 = from t in db.Trades
                       join h in db.Histories on t.Id equals h.TradeId
                       where h.Id == t.Histories.Max(i=>i.Id)
                       select new { t, h };

            if (filter.Status.HasValue == true)
            {
                var status = (int)filter.Status.Value;
                rst1 = rst1.Where(t => t.h.Status == status);
            }

            if (filter.AuthorId.HasValue == true)
            {
                rst1 = rst1.Where(t => t.t.Author == filter.AuthorId);
            }

            if (filter.LastModificationAfter.HasValue == true)
            {
                rst1 = rst1.Where(t => t.h.Created >= filter.LastModificationAfter);
            }

            if (filter.ModificationAuthor.HasValue == true)
            {
                rst1 = rst1.Where(t => t.h.Author == filter.ModificationAuthor);
            }

            var result = rst1
                .Select(x => x.t)
                .OrderBy(x => x.Id)
                .Where(x => x.Id >= startIndex)
                .Take(count);

            return result;

        }

        private IEnumerable<Trade> LoadTrades(TCEntities db, int startIndex, int count)
        {
            return db.Trades
                    .OrderBy(t => t.Id)
                    .Skip(startIndex)
                    .Take(count);
        }

        public ITradeCapture GetTrade(int tradeId)
        {
            ITradeCapture result;
            using (var e = new TCEntities())
            {
                var trade = e.Trades.Where(t => t.Id == tradeId).Single();
                result = this.CreateTrade(trade);
            }
            return result;
        }

        public void Save(ITradeCapture trade)
        {
            if (this.SaveNeeded(trade) == true)
            {
                this.AddHistoryToTrade(trade);
                RefreshHistory(trade);
            }
        }

        private void RefreshHistory(ITradeCapture trade)
        {
            List<ITradeHistory> history = new List<ITradeHistory>(trade.Histories);
            history.Add(TradeHistory.GetCopyOf(trade.Current));
            (trade as TradeCapture).Histories = history;
        }

        public static ITradeHistory GetLastTradeHistory(IEnumerable<ITradeHistory> historyCollection)
        {
            if (historyCollection != null && historyCollection.Count() > 0)
            {
                return historyCollection.OrderBy(h => h.Created).Reverse().First();
            }
            throw new ArgumentException("historyCollection");
        }


        private IList<ITradeListElemet> MakeListOfTradeListElements(IEnumerable<Trade> tradeSet)
        {
            List<ITradeListElemet> result = new List<ITradeListElemet>();
            foreach (var trade in tradeSet)
            {
                result.Add(this.CreateTradeListElement(trade));
            }
            return result;
        }

        private ITradeListElemet CreateTradeListElement(Trade trade)
        {
            List<ITradeHistory> history = new List<ITradeHistory>();
            foreach (var historyItem in trade.Histories)
            {
                history.Add(this.CreateTradeHistory(historyItem));
            }

            var element = new TradeListElement
            {
                Histories = history,
                Author = trade.User.Username,
                Crated = trade.Created,
                TradeId = trade.Id
            };

            return element;
        }

        private ITradeHistory CreateTradeHistory(History historyItem)
        {
            var th = new TradeHistory
            {
                Author = historyItem.User.Username,
                Created = historyItem.Created,
                Status = (TradeStatus)historyItem.Status,

                Annualization = historyItem.Annualization,
                AnnualizationEnabled = historyItem.AnnualizationEnabled,
                Bit = historyItem.Bit,
                CalculatorDelta = historyItem.CalculatorDelta,
                CalculatorFWD = historyItem.CalculatorFWD,
                CalculatorIRCorrection = historyItem.CalculatorIRCorrection,
                CalculatorMaxString = historyItem.CalculatorMaxString,
                CalculatorMinStrike = historyItem.CalculatorMinStrike,
                CalculatorName = historyItem.CalculatorName,
                CalculatorStrike = historyItem.CalculatorStrike,
                CalculatorStrikeUsed = historyItem.CalculatorStrikeUsed,
                CalculatorVolOfVol = historyItem.CalculatorVolOfVol,
                CalculatorVolSpread = historyItem.CalculatorVolSpread,
                Composite = historyItem.Composite,
                Contract = historyItem.Contract,
                DetailsAgent = historyItem.DetailsAgent,
                DetailsBook = historyItem.DetailsBook,
                DetailsBrokerCode = historyItem.DetailsBrokerCode,
                DetailsCounterBook = historyItem.DetailsCounterBook,
                DetailsCptyCode = historyItem.DetailsCptyCode,
                DetailsMarketer = historyItem.DetailsMarketer,
                DetailsTrader = historyItem.DetailsTrader,
                EconomicComments = historyItem.EconomicComments,
                EffectiveDate = historyItem.EffectiveDate,
                ExpectedN = historyItem.ExpectedN,
                ExpectedNEnabled = historyItem.ExpectedNEnabled,
                Expiry = historyItem.Expiry,
                ExpiryDataCallendar = historyItem.ExpiryDataCallendar,
                ExpiryDate = historyItem.ExpiryDate,
                ExpiryOffset = historyItem.ExpiryOffset,
                FixFinalParamEnabled = historyItem.FixFinalParamEnabled,
                FixFinalSetting = historyItem.FixFinalSetting,
                FixFirstParamEnabled = historyItem.FixFirstParamEnabled,
                FixFirstSetting = historyItem.FixFirstSetting,
                Frequency = historyItem.Frequency,
                Id = historyItem.Id,
                IndexName = historyItem.IndexName,
                InitialiExchange = historyItem.InitialiExchange,
                ManageRisk = historyItem.ManageRisk,
                MultipleExchange = historyItem.MultipleExchange,
                NationalCurrency = historyItem.NationalCurrency,
                Nominal = historyItem.Nominal,
                NominalMarketDisrupt = historyItem.NominalMarketDisrupt,
                NonDeliverable = historyItem.NonDeliverable,
                NonDeliverableEnabled = historyItem.NonDeliverableEnabled,
                ObsoleteCondition = historyItem.ObsoleteCondition,
                PaymentCallendar = historyItem.PaymentCallendar,
                PaymentCurrency = historyItem.PaymentCurrency,
                PaymentDate = historyItem.PaymentDate,
                PaymentLag = historyItem.PaymentLag,
                Periods = historyItem.Periods,
                ProductType = historyItem.ProductType,
                ReferenceVolatility = historyItem.ReferenceVolatility,
                RegularDatesFirst = historyItem.RegularDatesFirst,
                RegulatDatesLast = historyItem.RegulatDatesLast,
                RollDay = historyItem.RollDay,
                RollingMCD = historyItem.RollingMCD,
                Selling = historyItem.Selling,
                Strike = historyItem.Strike,
                StructureType = historyItem.StructureType,
                SubstractDays = historyItem.SubstractDays,
                SwapType = historyItem.SwapType,
                TradeId = historyItem.TradeId,
                Units = historyItem.Units,
                VegaAmount = historyItem.VegaAmount,
                VolumeCapture = historyItem.VolumeCapture,
                VolumeCaptureEnabled = historyItem.VolumeCaptureEnabled
            };

            return th;
        }

        private ITradeCapture CreateTrade(Trade dbTrade)
        {
            List<ITradeHistory> history = new List<ITradeHistory>();

            TradeCapture tcapture = new TradeCapture
            {
                AuthorId = dbTrade.Author,
                Created = dbTrade.Created,
                TradeId = dbTrade.Id,
                ViewId = dbTrade.View
            };

            foreach (var hItem in dbTrade.Histories)
            {
                history.Add(this.CreateTradeHistory(hItem));
            }

            tcapture.Histories = history;

            tcapture.Current = TradeHistory.GetCopyOf(GetLastTradeHistory(tcapture.Histories));

            return tcapture;
        }

        private void AddHistoryToTrade(ITradeCapture trade)
        {
            using (var e = new TCEntities())
            {
                var statusid = trade.Current.Status;
                var newhistory = History.CreateHistory(-1, trade.Current.AuthorId, DateTime.Now, trade.TradeId, (int)statusid);
                this.UpdateHistoryFields(trade.Current, newhistory);
                e.Histories.AddObject(newhistory);
                e.SaveChanges();
            }
        }

        private void UpdateHistoryFields(TradeHistory tradeHistory, History newhistory)
        {
            newhistory.VegaAmount = tradeHistory.VegaAmount;

            newhistory.Strike = tradeHistory.Strike;
            newhistory.VolumeCapture = tradeHistory.VolumeCapture;
            newhistory.Nominal = tradeHistory.Nominal;
            newhistory.ReferenceVolatility = tradeHistory.ReferenceVolatility;
            newhistory.CalculatorMinStrike = tradeHistory.CalculatorMinStrike;
            newhistory.CalculatorMaxString = tradeHistory.CalculatorMaxString;
            newhistory.Units = tradeHistory.Units;

            newhistory.Selling = tradeHistory.Selling;
            newhistory.AnnualizationEnabled = tradeHistory.AnnualizationEnabled;
            newhistory.FixFinalParamEnabled = tradeHistory.FixFinalParamEnabled;
            newhistory.ExpectedNEnabled = tradeHistory.ExpectedNEnabled;
            newhistory.VolumeCaptureEnabled = tradeHistory.VolumeCaptureEnabled;
            newhistory.FixFirstParamEnabled = tradeHistory.FixFirstParamEnabled;
            newhistory.Composite = tradeHistory.Composite;
            newhistory.NonDeliverableEnabled = tradeHistory.NonDeliverableEnabled;
            newhistory.InitialiExchange = tradeHistory.InitialiExchange;
            newhistory.CalculatorIRCorrection = tradeHistory.CalculatorIRCorrection;
            newhistory.CalculatorFWD = tradeHistory.CalculatorFWD;
            newhistory.MultipleExchange = tradeHistory.MultipleExchange;
            newhistory.CalculatorDelta = tradeHistory.CalculatorDelta;
            newhistory.NominalMarketDisrupt = tradeHistory.NominalMarketDisrupt;
            newhistory.ManageRisk = tradeHistory.ManageRisk;

            newhistory.FixFirstSetting = tradeHistory.FixFirstSetting;
            newhistory.ExpectedN = tradeHistory.ExpectedN;
            newhistory.PaymentLag = tradeHistory.PaymentLag;
            newhistory.FixFinalSetting = tradeHistory.FixFinalSetting;
            newhistory.RollDay = tradeHistory.RollDay;
            newhistory.CalculatorStrike = tradeHistory.CalculatorStrike;
            newhistory.Periods = tradeHistory.Periods;
            newhistory.Annualization = tradeHistory.Annualization;
            newhistory.CalculatorStrikeUsed = tradeHistory.CalculatorStrikeUsed;
            newhistory.CalculatorVolOfVol = tradeHistory.CalculatorVolOfVol;
            newhistory.CalculatorVolSpread = tradeHistory.CalculatorVolSpread;

            newhistory.SubstractDays = tradeHistory.SubstractDays;
            newhistory.ExpiryOffset = tradeHistory.ExpiryOffset;

            newhistory.EffectiveDate = tradeHistory.EffectiveDate;
            newhistory.RegularDatesFirst = tradeHistory.RegularDatesFirst;
            newhistory.RegulatDatesLast = tradeHistory.RegulatDatesLast;
            newhistory.ExpiryDate = tradeHistory.ExpiryDate;
            newhistory.PaymentDate = tradeHistory.PaymentDate;

            newhistory.NationalCurrency = tradeHistory.NationalCurrency;
            newhistory.NonDeliverable = tradeHistory.NonDeliverable;
            newhistory.ObsoleteCondition = tradeHistory.ObsoleteCondition;
            newhistory.Bit = tradeHistory.Bit;
            newhistory.Frequency = tradeHistory.Frequency;
            newhistory.RollingMCD = tradeHistory.RollingMCD;
            newhistory.CalculatorName = tradeHistory.CalculatorName;
            newhistory.ExpiryDataCallendar = tradeHistory.ExpiryDataCallendar;
            newhistory.DetailsBook = tradeHistory.DetailsBook;
            newhistory.DetailsTrader = tradeHistory.DetailsTrader;
            newhistory.DetailsMarketer = tradeHistory.DetailsMarketer;
            newhistory.DetailsCptyCode = tradeHistory.DetailsCptyCode;
            newhistory.DetailsCounterBook = tradeHistory.DetailsCounterBook;
            newhistory.DetailsAgent = tradeHistory.DetailsAgent;
            newhistory.DetailsBrokerCode = tradeHistory.DetailsBrokerCode;
            newhistory.EconomicComments = tradeHistory.EconomicComments;
            newhistory.Expiry = tradeHistory.Expiry;
            newhistory.PaymentCallendar = tradeHistory.PaymentCallendar;
            newhistory.PaymentCurrency = tradeHistory.PaymentCurrency;
            newhistory.Contract = tradeHistory.Contract;
            newhistory.ProductType = tradeHistory.ProductType;
            newhistory.StructureType = tradeHistory.StructureType;
            newhistory.IndexName = tradeHistory.IndexName;
            newhistory.SwapType = tradeHistory.SwapType;
        }

        private bool SaveNeeded(ITradeCapture trade)
        {
            return GetLastTradeHistory(trade.Histories).SaveNeeded(trade.Current);
        }

        public int GetTradeCount(TradeFilter filter = null)
        {
            using (var db = new TCEntities())
            {
                if (filter != null)
                {
                    var tradesToCount = from t in db.Trades
                                        join h in db.Histories on t.Id equals h.TradeId
                                        where h.Id == t.Histories.Max(i => i.Id)
                                        select new { t, h };

                    if (filter.Status.HasValue == true)
                    {
                        var status = (int)filter.Status.Value;
                        tradesToCount = tradesToCount.Where(t => t.h.Status == status);
                    }

                    if (filter.AuthorId.HasValue == true)
                    {
                        tradesToCount = tradesToCount.Where(t => t.t.Author == filter.AuthorId);
                    }

                    if (filter.LastModificationAfter.HasValue == true)
                    {
                        tradesToCount = tradesToCount.Where(t => t.h.Created >= filter.LastModificationAfter);
                    }

                    if (filter.ModificationAuthor.HasValue == true)
                    {
                        tradesToCount = tradesToCount.Where(t => t.h.Author == filter.ModificationAuthor);
                    }
                    return tradesToCount.Count();
                }
                else
                {
                    return db.Trades.Count();
                }
            }
        }


        public ITradeUser LoadUserByName(string username)
        {
            ITradeUser user = null;
            using (var db = new TCEntities())
            {
                var dbuser = db.Users.Where(u => u.Username == username).FirstOrDefault();
                if (dbuser != null)
                {
                    user = new TradeUser
                    {
                        Id = dbuser.Id,
                        Username = dbuser.Username.Trim(),
                        HashPassword = dbuser.Userpassword.Trim(),
                        Account = (AccountType)dbuser.Account,
                        UserPicture = dbuser.Image,
                        FirstName = dbuser.FirstName,
                        LastName = dbuser.LastName
                    };
                }
            }

            return user;
        }



    }
}
