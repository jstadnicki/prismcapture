﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;

namespace Capture.Module.Trades.Messages
{
    public enum DocumentModificationStatus
    {
        Opened = 1,
        Modified,
        Saved
    }


    public class EditTradeDocumentStatus : CompositePresentationEvent<DocumentModificationStatus>
    {
    }


}
