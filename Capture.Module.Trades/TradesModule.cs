﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using Capture.Module.Trades.MVVM;
using Capture.Common.Prism;

namespace Capture.Module.Trades
{
    public sealed class TradesModule : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;
        
        public TradesModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<ICommandsTradeView, CommandsTradeView>();
            this.container.RegisterType<ICommandsTradeViewModel, CommandsTradeViewModel>();

            this.container.RegisterType<object, EditTradeView>(typeof(IEditTradeView).FullName);
            this.container.RegisterType<IEditTradeViewModel, EditTradeViewModel>();

            this.container.RegisterType<object, LoadTradeView>(typeof(ILoadTradeView).FullName);
            this.container.RegisterType<ILoadTradeViewModel, LoadTradeViewModel>();

            this.container.RegisterType<object, EmptyView>(typeof(IEmptyView).FullName);
            this.container.RegisterType<IEmptyViewModel, EmptyViewModel>();


            this.RegisterViews();
        }

        private void RegisterViews()
        {
            this.regionManager.NavigationService().RegisterViewWithRegion(Capture.Common.RegionNames.RightBarRegion,
                typeof(CommandsTradeView));

            this.regionManager.NavigationService().RegisterViewWithRegion(Capture.Common.RegionNames.CenterFillRegion,
               typeof(EmptyView));
            
            this.regionManager.NavigationService().RegisterViewWithRegion(Capture.Common.RegionNames.CenterFillRegion,
                typeof(EditTradeView));

            this.regionManager.NavigationService().RegisterViewWithRegion(Capture.Common.RegionNames.CenterFillRegion,
               typeof(LoadTradeView));
            
        }
    }
}
