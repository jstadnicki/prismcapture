﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using Microsoft.Practices.Prism.Regions;
using Capture.Common.Model;
using Capture.Common;
using Capture.Common.Repository;
using System.ComponentModel;
using Microsoft.Practices.Prism.Events;
using Capture.Module.Trades.Messages;
using Capture.Common.Prism;
using Capture.Common.Messages;
using Microsoft.Practices.Prism.Commands;


namespace Capture.Module.Trades.MVVM
{
    public sealed class EditTradeViewModel : ViewModelBase, IEditTradeViewModel, IConfirmNavigationRequest
    {
        private IRepository repository;
        private IEventAggregator eventAggregator;
        private Xceed.Wpf.Toolkit.WindowState saveOnCloseVisibility;
        private IRegionManager regionManager;
        private bool documentModified;
        private ITradeCapture currentTrade;

        private Action<bool> continuationCallback;
        private ITradeUser currentUser;

        public EditTradeViewModel(IRepository repository, IEventAggregator eventAggregator, IRegionManager regionManger, ITradeUser user)
        {
            this.repository = repository;
            this.eventAggregator = eventAggregator;
            this.DocumentModified = false;
            this.currentUser = user;
            this.regionManager = regionManger;
            this.eventAggregator.GetEvent<Capture.Common.Messages.TradeCaptureApplicationCommands>().Subscribe(this.OnApplicationCommandReceived);
            this.SaveOnCloseVisibility = Xceed.Wpf.Toolkit.WindowState.Closed;

            this.SaveChanges = new DelegateCommand(this.SaveChangesExecute);
            this.DropChanges = new DelegateCommand(this.DropChangesExecute);
            this.CancelNavigation = new DelegateCommand(this.CancelNavigationExecute);
        }

        private void SaveChangesExecute()
        {
            this.repository.Save(this.CurrentTrade);
            if (this.continuationCallback != null)
            {
                HandleUserNavigateInteraction(true);
            }
        }

        private void DropChangesExecute()
        {
            HandleUserNavigateInteraction(true);
        }

        private void CancelNavigationExecute()
        {
            HandleUserNavigateInteraction(false);
        }

        private void HandleUserNavigateInteraction(bool navigationAccepted)
        {
            this.continuationCallback(navigationAccepted);
            this.continuationCallback = null;
            this.SaveOnCloseVisibility = Xceed.Wpf.Toolkit.WindowState.Closed;
        }

        private void TradeChanged(object s, PropertyChangedEventArgs property)
        {
            this.eventAggregator.GetEvent<EditTradeDocumentStatus>().Publish(DocumentModificationStatus.Modified);
            this.CurrentTrade.Current.AuthorId = this.currentUser.Id;
            DocumentModified = true;
        }

        private void OnApplicationCommandReceived(Capture.Common.Messages.TradeApplicationCommand command)
        {
            switch (command.CommandType)
            {
                case Capture.Common.Messages.ApplicationCommandsType.SaveTrade:
                    this.HandleSaveTradeCommand();
                    break;
                case Capture.Common.Messages.ApplicationCommandsType.LoadTradeWithId:
                    this.HandleLoadTradeCommand(command);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Application command parameter");
            }
        }

        private void HandleLoadTradeCommand(TradeApplicationCommand command)
        {
            this.regionManager
                .NavigationService()
                .RequestNavigateWithTradeIdAsParam(typeof(IEditTradeView).FullName, command.LoadTradeId);
        }

        private void HandleSaveTradeCommand()
        {
            if (this.DocumentModified)
            {
                this.repository.Save(this.CurrentTrade);
                this.eventAggregator.GetEvent<EditTradeDocumentStatus>().Publish(DocumentModificationStatus.Saved);
                DocumentModified = false;
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return navigationContext.Parameters
                    .Where(k => k.Key == PareterNavigationNames.TradeId)
                    .Count() == 0;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var string_id = navigationContext.Parameters.Where(k => k.Key == PareterNavigationNames.TradeId).First();
            var int_id = int.Parse(string_id.Value);
            this.CurrentTrade = this.repository.GetTrade(int_id);
        }

        public bool DocumentModified
        {
            get { return this.documentModified; }
            set { this.documentModified = value; }
        }

        public void ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
        {
            if (this.DocumentModified)
            {
                this.SaveOnCloseVisibility = Xceed.Wpf.Toolkit.WindowState.Open;
                this.continuationCallback = continuationCallback;
            }
            else
            {
                continuationCallback(true);
            }
        }

        public Xceed.Wpf.Toolkit.WindowState SaveOnCloseVisibility
        {
            get
            {
                return this.saveOnCloseVisibility;
            }
            set
            {
                this.saveOnCloseVisibility = value;
                OnPropertyChanged("SaveOnCloseVisibility");
            }
        }

        public DelegateCommand SaveChanges
        {
            get;
            private set;
        }

        public DelegateCommand DropChanges
        {
            get;
            private set;
        }

        public ITradeCapture CurrentTrade
        {
            get { return this.currentTrade; }
            set
            {
                if (this.currentTrade == value)
                {
                    return;
                }
                if (this.currentTrade != null)
                {
                    this.currentTrade.PropertyChanged -= this.TradeChanged;
                }

                this.currentTrade = value;
                this.currentTrade.PropertyChanged += this.TradeChanged;
                this.eventAggregator.GetEvent<EditTradeDocumentStatus>().Publish(DocumentModificationStatus.Opened);
            }
        }



        public DelegateCommand CancelNavigation
        {
            get;
            private set;
        }
    }
}
