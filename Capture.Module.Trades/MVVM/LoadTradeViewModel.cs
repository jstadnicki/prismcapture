﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using Capture.Common;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism;
using System.Windows.Threading;
using Capture.Common.Model;
using System.Collections.ObjectModel;
using Capture.Common.Repository;
using System.Threading;
using System.Windows;
using Capture.Common.Prism;

namespace Capture.Module.Trades.MVVM
{
    public sealed class LoadTradeViewModel : ViewModelBase, ILoadTradeViewModel, INavigationAware
    {
        public ObservableCollection<ITradeListElemet> TradesList
        {
            get;
            set;
        }

        public LoadTradeViewModel(IRepository repository, IRegionManager regionManager)
        {
            // dependecies
            this.repository = repository;
            this.regionManager = regionManager;


            this.CreateCommands();
            this.currentItemsPerPage = 10;
            this.SelectedTradeIndex = -1;
            this.PageList = new List<int> { 0 };
            this.ItemsPerPage = new List<int> { 10, 25, 50, 100 };
            this.TradesList = new ObservableCollection<ITradeListElemet>();
        }

        private void CreateCommands()
        {
            CancelTradeLoadCommand = new DelegateCommand(CancelTradeLoadExecute, CancelTradeLoadCanExecute);
            LoadSelectedTradeCommand = new DelegateCommand(LoadTradeExecute, LoadTradeCanExecute);
            NextTradesCommand = new DelegateCommand(NextTradesExecute, NextTradesCanExecute);
            PrevTradesCommand = new DelegateCommand(PrevTradesExecute, PrevTradesCanExecute);
        }

        void CancelTradeLoadExecute()
        {
            this.regionManager
                .NavigationService()
                .RequestNavigateWithTradeIdAsParam(typeof(IEmptyView).FullName);
        }

        bool CancelTradeLoadCanExecute()
        {
            return true;
        }

        void LoadTradeExecute()
        {
            this.regionManager
                .NavigationService()
                .RequestNavigateWithTradeIdAsParam(typeof(IEditTradeView).FullName, this.TradesList.ElementAt(this.SelectedTradeIndex).TradeId);
        }

        bool LoadTradeCanExecute()
        {
            return this.SelectedTradeIndex != -1;
        }

        void NextTradesExecute()
        {
            this.CurrentPage++;
        }

        private void FetchDBTradesAsync()
        {
            var r = this.repository.GetTradeList((this.CurrentPage - 1) * this.CurrentItemsPerPage, this.CurrentItemsPerPage);

            this.CurrentDispatcher.Invoke(new Action(() =>
            {
                this.TradesList.Clear();
                this.TradesList.AddRange(r);
                this.UpdatePageCount();
                this.SelectedTradeIndex = -1;
                this.IsBusy = false;
            }));
        }

        bool NextTradesCanExecute()
        {
            return this.CurrentPage < this.PageList.Last();
        }

        void PrevTradesExecute()
        {
            this.CurrentPage--;

        }

        bool PrevTradesCanExecute()
        {
            return this.CurrentPage > 1;
        }

        public DelegateCommand CancelTradeLoadCommand
        {
            get;
            private set;
        }

        public string BusyIndicatorText
        {
            get { return "Loading data - please wait"; }
        }

        bool isBusy;
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }
            set
            {
                this.isBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        public DelegateCommand LoadSelectedTradeCommand
        {
            get;
            private set;
        }

        public DelegateCommand NextTradesCommand
        {
            get;
            private set;
        }

        public DelegateCommand PrevTradesCommand
        {
            get;
            private set;
        }

        int currentPage;
        public int CurrentPage
        {
            get
            {
                return this.currentPage;
            }
            set
            {
                this.currentPage = value;
                RefreshDataForCurrentPage();
                OnPagesNavigationAvaialablilityChanged();
                OnPropertyChanged("CurrentPage");
            }
        }

        private void RefreshDataForCurrentPage()
        {
            this.IsBusy = true;
            ThreadPool.QueueUserWorkItem((o) =>
            {
                FetchDBTradesAsync();
            });
        }

        private void OnPagesNavigationAvaialablilityChanged()
        {
            this.NextTradesCommand.RaiseCanExecuteChanged();
            this.PrevTradesCommand.RaiseCanExecuteChanged();
        }

        public int PagesCount
        {
            get
            {
                return PageList.Count();
            }
            set
            {
                PageList = new List<int>(Enumerable.Range(1, value));
                OnPropertyChanged("PagesCount");
                OnPagesNavigationAvaialablilityChanged();
            }
        }

        List<int> pageList;
        public IEnumerable<int> PageList
        {
            get
            {
                return this.pageList;
            }
            set
            {
                this.pageList = new List<int>(value);
                OnPropertyChanged("PageList");
            }
        }


        private IRepository repository;
        List<int> itemsPerPage;
        public IEnumerable<int> ItemsPerPage
        {
            get
            {
                return this.itemsPerPage;
            }
            set
            {
                this.itemsPerPage = new List<int>(value);
                OnPropertyChanged("ItemsPerPage");
            }
        }

        int currentItemsPerPage;
        private IRegionManager regionManager;
        public int CurrentItemsPerPage
        {
            get
            {
                return this.currentItemsPerPage;
            }
            set
            {
                int oldvalue = this.currentItemsPerPage;
                this.currentItemsPerPage = value;
                this.UpdatePageCount();
                OnPropertyChanged("CurrentItemsPerPage");

                RefreshCurrentlyDisplayedItems(oldvalue);

            }
        }

        private void RefreshCurrentlyDisplayedItems(int oldvalue)
        {
            int tradeToInclude = Math.Max(0, CurrentPage - 1) * oldvalue;
            var pagestart = tradeToInclude / this.CurrentItemsPerPage;
            this.currentPage = pagestart;
            this.NextTradesExecute();

        }

        private void UpdatePageCount()
        {
            int tcount = this.repository.GetTradeCount();
            this.PagesCount = Math.Max(1, tcount / CurrentItemsPerPage);
            if (PagesCount * CurrentItemsPerPage < tcount)
            {
                this.PagesCount++;
            }
        }


        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            this.TradesList.Clear();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            this.NextTradesExecute();
        }

        int selectedTradeIndex;
        public int SelectedTradeIndex
        {
            get
            { return this.selectedTradeIndex; }
            set
            {
                this.selectedTradeIndex = value;
                OnPropertyChanged("SelectedTradeIndex");
                this.LoadSelectedTradeCommand.RaiseCanExecuteChanged();
            }
        }
    }
}
