﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using Microsoft.Practices.Prism.Commands;

namespace Capture.Module.Trades.MVVM
{
    public interface ILoadTradeViewModel
    {
        DelegateCommand CancelTradeLoadCommand { get; }
        DelegateCommand LoadSelectedTradeCommand { get; }
        DelegateCommand NextTradesCommand { get; }
        DelegateCommand PrevTradesCommand { get; }
        string BusyIndicatorText { get; }
        bool IsBusy { get; }
        int CurrentPage { get; }
        IEnumerable<int> ItemsPerPage { get; }

        int PagesCount { get; }
        IEnumerable<int> PageList { get; }
        int CurrentItemsPerPage { get; set; }
        

    }
}
