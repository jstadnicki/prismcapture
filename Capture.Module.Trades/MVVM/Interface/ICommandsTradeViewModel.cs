﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using Microsoft.Practices.Prism.Commands;

namespace Capture.Module.Trades.MVVM
{
    public interface ICommandsTradeViewModel 
    {
        DelegateCommand LoadTradeCommand { get; }
        DelegateCommand SaveTradeCommand { get; }
    }
}
