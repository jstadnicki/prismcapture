﻿using System;
using System.Collections.Generic;
using Capture.Common.Model;
using Microsoft.Practices.Prism.Commands;

namespace Capture.Module.Trades.MVVM
{
    public interface IEditTradeViewModel
    {
        ITradeCapture CurrentTrade { get; }
        Xceed.Wpf.Toolkit.WindowState SaveOnCloseVisibility { get; set; }
        DelegateCommand SaveChanges { get; }
        DelegateCommand DropChanges { get; }
        DelegateCommand CancelNavigation { get; }
    }
}
