﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;

namespace Capture.Module.Trades.MVVM
{
    public interface IEmptyViewModel
    {
        double FontSize { get; }
        string Text { get; }
        SolidColorBrush Foreground { get; }
        TextWrapping TextWrapping { get; }
    }
}
