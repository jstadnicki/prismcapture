﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Capture.Common.MVVM;

namespace Capture.Module.Trades.MVVM
{
    /// <summary>
    /// Interaction logic for LoadTradeView.xaml
    /// </summary>
    public partial class LoadTradeView : UserControl, ILoadTradeView
    {
        public LoadTradeView(ILoadTradeViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        public ViewModelBase ViewModel
        {
            get
            {
                return (ViewModelBase)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
