﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;

namespace Capture.Module.Trades.MVVM
{
    public class EmptyViewModel : Common.MVVM.ViewModelBase, IEmptyViewModel
    {
        public EmptyViewModel()
        {
            this.FontSize = 40;
            this.Text = "No trade was loaded.\nImport, load or create in order to proceed.";
            this.Foreground = new SolidColorBrush(Colors.DarkGray);
            this.TextWrapping = System.Windows.TextWrapping.Wrap;
        }
        public double FontSize{get;set;}
        public string Text { get; set; }
        public SolidColorBrush Foreground { get; set; }
        public TextWrapping TextWrapping { get; set; }
    }
}
