﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Capture.Common;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Events;
using Capture.Module.Trades.Messages;
using Capture.Common.Messages;

namespace Capture.Module.Trades.MVVM
{
    public sealed class CommandsTradeViewModel : ViewModelBase, ICommandsTradeViewModel
    {
        public CommandsTradeViewModel(IRegionManager regionManager, IEventAggregator eventAggregator)            
        {
            this.CreateCommands();
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
        }

        private void CreateCommands()
        {
            this.LoadTradeCommand = new DelegateCommand(LoadTradeCommandExecute, LoadTradeCommandCanExecute);
            this.SaveTradeCommand = new DelegateCommand(SaveTradeCommandExecute);
        }


        public DelegateCommand LoadTradeCommand
        {
            get;
            private set;
        }

        private void SaveTradeCommandExecute()
        {
            this.eventAggregator
                .GetEvent<TradeCaptureApplicationCommands>()
                .Publish(new TradeApplicationCommand 
                    { CommandType = ApplicationCommandsType.SaveTrade });
        }


        private void LoadTradeCommandExecute()
        {
            var uriQuery = new UriQuery();
            var uri = new Uri(typeof(LoadTradeView).FullName, UriKind.Relative);
            this.regionManager.RequestNavigate(RegionNames.CenterFillRegion, uri);
        }

        private bool LoadTradeCommandCanExecute()
        {
            var v = this.regionManager.Regions[RegionNames.CenterFillRegion].ActiveViews;
            var r = v.Where(av => av.GetType() == typeof(LoadTradeView)).FirstOrDefault();
            return r == null;
        }


        IRegionManager regionManager;
        private IEventAggregator eventAggregator;


        public DelegateCommand SaveTradeCommand
        {
            get;
            private set;
        }
    }
}
