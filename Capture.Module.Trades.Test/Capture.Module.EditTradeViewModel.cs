﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using Capture.Module.Trades.MVVM;
using Capture.Common.MVVM;
using Capture.Common.Repository;
using System.Threading;
using Capture.Common.Model;
using System.Windows;
using Capture.Common;
using System.Windows.Threading;
using Microsoft.Practices.Prism.Regions;
using Capture.Common.Prism;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Events;
using Capture.Module.Trades.Messages;
using Capture.Common.Messages;

namespace Capture.Module.Trades.Test
{
    [TestFixture, RequiresSTA]
    public sealed class EditTradeViewModelTest
    {
        UnityContainer container;

        public static int RepositoryMock_TradeCaptureID;
        public static bool RepositoryMock_SaveRequested;
        public static ITradeCapture RepositoryMock_TradeCapture;

        [TestFixtureSetUp]
        public void ArrangeTests()
        {
            this.container = new UnityContainer();
            container.RegisterType<IEditTradeViewModel, EditTradeViewModel>();
            container.RegisterType<IRepository, EditTradeRepositoryMock>();
            container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());
            container.RegisterType<IRegionManager, EditTradeRegionManagerMock>();
            TradeUser tu = new TradeUser
            {
                Account = AccountType.Junior,
                FirstName = "FirstName",
                HashPassword = "asdasdasd",
                Id = 1,
                LastName = "Lastname",
                Username = "testuser",
                UserPicture = null
            };

            this.container.RegisterInstance<ITradeUser>(tu);
        }

        [Test]
        public void T000_EditTradeViewModel_Should_Implement_IEditTradeViewModel()
        {
            // arrange
            // act
            var sut = this.container.Resolve<IEditTradeViewModel>();

            // assert
            Assert.NotNull(sut);
        }

        [Test]
        public void T001_When_Initialized_ETVM_Should_Initialize_Properties_Properly()
        {
            // arrange
            // act
            var sut = this.container.Resolve<IEditTradeViewModel>();

            // assert
            Assert.Null(sut.CurrentTrade);
            Assert.NotNull(sut.DropChanges);
            Assert.NotNull(sut.SaveChanges);
            Assert.NotNull(sut.CancelNavigation);
            Assert.That(sut.SaveOnCloseVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Closed));
        }

        [Test]
        public void T002_When_Navigated_To_With_Valid_Trade_Id_In_Param_Should_Load_Trade_From_Repository_And_Set_It_To_Current_Trade()
        {
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();

            // act
            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);
            NavigationContext nc = new NavigationContext(null, uri);

            sut.OnNavigatedTo(nc);

            // assert
            Assert.NotNull(sut.CurrentTrade);
            Assert.That(sut.CurrentTrade.TradeId, Is.EqualTo(RepositoryMock_TradeCaptureID));
        }

        [Test]
        public void T003_When_UserProvides_At_Least_One_Modification_To_The_Document_Should_Broadcast_Message_With_Informatino_That_The_Document_Is_Modified()
        {
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();
            var evMsg = this.container.Resolve<IEventAggregator>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);
            NavigationContext nc = new NavigationContext(null, uri);

            sut.OnNavigatedTo(nc);

            bool result = false;


            var msgType = evMsg.GetEvent<EditTradeDocumentStatus>()
                .Subscribe(new Action<DocumentModificationStatus>(s =>
            {
                if (s == DocumentModificationStatus.Modified)
                {
                    result = true;
                }
            }));

            // act
            sut.CurrentTrade.Current.CalculatorMaxString = 676;

            // assert
            Assert.True(result);
            Assert.True(sut.DocumentModified);

        }

        [Test]
        public void T004_When_Navigated_To_Edit_Trade_View_With_Valid_Id_After_Document_Is_Set_Shoud_Broadcast_A_DocumentModificationStatus_With_Value_Opened()
        {
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();
            var evMsg = this.container.Resolve<IEventAggregator>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);
            NavigationContext nc = new NavigationContext(null, uri);


            bool result = false;


            var msgType = evMsg.GetEvent<EditTradeDocumentStatus>()
                .Subscribe(new Action<DocumentModificationStatus>(s =>
                {
                    if (s == DocumentModificationStatus.Opened)
                    {
                        result = true;
                    }
                }));

            // act
            sut.OnNavigatedTo(nc);


            // assert
            Assert.True(result);
            Assert.False(sut.DocumentModified);
        }

        [Test]
        public void T005_When_User_Did_Not_Introduced_Any_Changed_In_Document_And_Request_For_Save_Was_Sent_Should_Not_Trigger_Saving_Operation()
        {
            this.container.Dispose();
            this.ArrangeTests();
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();
            var evMsg = this.container.Resolve<IEventAggregator>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);
            NavigationContext nc = new NavigationContext(null, uri);


            bool result = false;
            EditTradeViewModelTest.RepositoryMock_SaveRequested = false;

            var msgType = evMsg.GetEvent<EditTradeDocumentStatus>()
                .Subscribe(new Action<DocumentModificationStatus>(s =>
                {
                    if (s == DocumentModificationStatus.Saved)
                    {
                        result = true;
                    }
                }));
            sut.OnNavigatedTo(nc);

            // act
            evMsg.GetEvent<TradeCaptureApplicationCommands>().Publish(new TradeApplicationCommand { CommandType = ApplicationCommandsType.SaveTrade });

            // assert
            Assert.False(result);
            Assert.False(sut.DocumentModified);
            Assert.False(RepositoryMock_SaveRequested);
        }

        [Test]
        public void T006_When_User_Did_Introduced_Changes_In_Document_And_Requested_Save_EditTrade_Should_Trigger_Save_Operation_And_After_Save_Message_Should_Be_Sent()
        {
            this.container.Dispose();
            this.ArrangeTests();
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();
            var evMsg = this.container.Resolve<IEventAggregator>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);
            NavigationContext nc = new NavigationContext(null, uri);


            bool saved = false;
            bool modified = false;
            EditTradeViewModelTest.RepositoryMock_SaveRequested = false;

            evMsg.GetEvent<EditTradeDocumentStatus>().Subscribe(new Action<DocumentModificationStatus>(s =>
                {
                    if (s == DocumentModificationStatus.Saved)
                    {
                        saved = true;
                    }
                }));

            evMsg.GetEvent<EditTradeDocumentStatus>().Subscribe(new Action<DocumentModificationStatus>(s =>
                {
                    if (s == DocumentModificationStatus.Modified)
                    {
                        modified = true;
                    }
                }));
            sut.OnNavigatedTo(nc);

            // act
            sut.CurrentTrade.Current.CalculatorMaxString = 987;
            Assert.True(modified);
            Assert.True(sut.DocumentModified);

            evMsg.GetEvent<TradeCaptureApplicationCommands>()
                .Publish(new TradeApplicationCommand { CommandType = ApplicationCommandsType.SaveTrade });

            // assert
            Assert.True(saved);
            Assert.False(sut.DocumentModified);
            Assert.True(RepositoryMock_SaveRequested);
        }

        [Test]
        public void T007_When_Document_Were_Modified_Navigation_Away_From_Edit_View_Should_Display_A_Window_With_Question_About_The_Changes()
        {
            this.container.Dispose();
            this.ArrangeTests();
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;

            // act
            sut.ConfirmNavigationRequest(null, _ =>
                {
                    //ignore the bool value, will be tested in another test
                });

            // assert
            Assert.That(sut.SaveOnCloseVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Open));
        }

        [Test]
        public void T008_When_Changes_Were_Introduced_And_User_Did_NOT_Press_OK_or_NO_Edit_Should_Not_Navigate_Away()
        {
            this.container.Dispose();
            this.ArrangeTests();
            //Capture.Common.Prism.PrismExtensions.ServiceFactory = f => new EditTradeNavigationServiceMock();
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;

            ManualResetEvent mre = new ManualResetEvent(false);

            bool wascalled = false;
            // act
            sut.ConfirmNavigationRequest(null, b =>
            {
                mre.Set();
                wascalled = true;
            });

            mre.WaitOne(3456);

            // assert
            Assert.False(wascalled);
        }

        [Test]
        public void T009_When_Changes_Were_Introduced_And_User_Choose_To_CANCEL_Navigation_Should_Canceled()
        {
            this.container.Dispose();
            this.ArrangeTests();
            //Capture.Common.Prism.PrismExtensions.ServiceFactory = f => new EditTradeNavigationServiceMock();
            // arrange
            var sut = this.container.Resolve<EditTradeViewModel>();

            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;

            ManualResetEvent mre = new ManualResetEvent(false);

            bool wascalled = false;
            bool callresult = true;
            
            sut.ConfirmNavigationRequest(null, b =>
            {
                mre.Set();
                wascalled = true;
                callresult = b;
            });

            // act

            sut.CancelNavigation.Execute();

            mre.WaitOne(250);

            // assert
            Assert.True(wascalled);
            Assert.False(callresult);
            Assert.That(sut.SaveOnCloseVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Closed));
        }

        [Test]
        public void T010_When_Changes_Were_Introduced_And_User_Choose_NO_Navigation_Should_Be_Accepted_And_SaveTrade_Should_Not_Be_Triggered()
        {
            // arrange
            this.container.Dispose();
            this.ArrangeTests();
            var sut = this.container.Resolve<EditTradeViewModel>();
            EditTradeViewModelTest.RepositoryMock_SaveRequested = false;
            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;

            ManualResetEvent mre = new ManualResetEvent(false);

            bool wascalled = false;
            bool callresult = true;

            sut.ConfirmNavigationRequest(null, b =>
            {
                mre.Set();
                wascalled = true;
                callresult = b;
            });

            // act

            sut.DropChanges.Execute();

            mre.WaitOne(250);

            // assert
            Assert.True(wascalled);
            Assert.True(callresult);
            Assert.False(EditTradeViewModelTest.RepositoryMock_SaveRequested);
            Assert.That(sut.SaveOnCloseVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Closed));
        }

        [Test]
        public void T011_When_Changes_Were_Introduced_And_User_Choose_YES_Navigation_Should_Be_Accepted_And_SaveTrade_Should_Be_Triggered()
        {
            // arrange
            this.container.Dispose();
            this.ArrangeTests();
            var sut = this.container.Resolve<EditTradeViewModel>();
            EditTradeViewModelTest.RepositoryMock_SaveRequested = false;
            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;

            ManualResetEvent mre = new ManualResetEvent(false);

            bool wascalled = false;
            bool callresult = true;

            sut.ConfirmNavigationRequest(null, b =>
            {
                mre.Set();
                wascalled = true;
                callresult = b;
            });

            // act

            sut.SaveChanges.Execute();

            mre.WaitOne(250);

            // assert
            Assert.True(wascalled);
            Assert.True(callresult);
            Assert.True(EditTradeViewModelTest.RepositoryMock_SaveRequested);
            Assert.That(sut.SaveOnCloseVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Closed));
        }

        [Test]
        public void T012_When_Change_Is_Introduced_Author_Of_Last_Modification_Should_Be_The_Current_Logged_User()
        {
            // arrange
            this.container.Dispose();
            this.ArrangeTests();
            var sut = this.container.Resolve<EditTradeViewModel>();
            EditTradeViewModelTest.RepositoryMock_SaveRequested = false;
            RepositoryMock_TradeCaptureID = 3;
            var uriq = new UriQuery();
            uriq.Add(PareterNavigationNames.TradeId, RepositoryMock_TradeCaptureID.ToString());
            var uri = new Uri(typeof(IEditTradeView).FullName + uriq, UriKind.Relative);

            NavigationContext nc = new NavigationContext(null, uri);
            sut.OnNavigatedTo(nc);

            sut.CurrentTrade.Current.CalculatorMaxString = 987;
            var user = this.container.Resolve<ITradeUser>();


            // act
            sut.SaveChanges.Execute();

            // assert

            Assert.That(EditTradeViewModelTest.RepositoryMock_TradeCapture.Current.AuthorId, Is.EqualTo(user.Id));
        }
    }

    public class EditTradeRegionManagerMock : IRegionManager
    {
        public IRegionManager CreateRegionManager()
        {
            throw new NotImplementedException();
        }

        public IRegionCollection Regions
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class EditTradeNavigationServiceMock : INavigation
    {
        public IRegionManager AddToRegion(string regionName, object view)
        {
            throw new NotImplementedException();
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Func<object> getContentDelegate)
        {
            throw new NotImplementedException();
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Type viewType)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, string source)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, Uri source)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, string source, Action<NavigationResult> navigationCallback)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, Uri source, Action<NavigationResult> navigationCallback)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigateWithTradeIdAsParam(string baseAddress, int id = -1)
        {
            throw new NotImplementedException();
        }
    }



    public class EditTradeRepositoryMock : IRepository
    {
        public IEnumerable<ITradeListElemet> GetTradeList(int startIndex = 0, int count = int.MaxValue, TradeFilter filter = null)
        {
            throw new NotImplementedException();
        }

        public ITradeCapture GetTrade(int tradeId)
        {
            var tc = new TradeCapture
            {
                TradeId = EditTradeViewModelTest.RepositoryMock_TradeCaptureID
            };

            List<TradeHistory> histories = new List<TradeHistory>();
            for (int i = 0; i < 4; i++)
            {
                histories.Add(new TradeHistory
                {
                    Id = i,
                    Created = DateTime.Now.AddDays(i)
                });
            }

            tc.Current = TradeHistory.GetCopyOf(histories.OrderBy(h => h.Created).Reverse().First());
            tc.Histories = histories;

            return tc;
        }

        public void Save(ITradeCapture trade)
        {
            EditTradeViewModelTest.RepositoryMock_SaveRequested = true;
            EditTradeViewModelTest.RepositoryMock_TradeCapture = trade;
        }

        public int GetTradeCount(TradeFilter filter = null)
        {
            throw new NotImplementedException();
        }

        public ITradeUser LoadUserByName(string username)
        {
            throw new NotImplementedException();
        }
    }
}