﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using Capture.Module.Trades.MVVM;
using Capture.Common.MVVM;
using Capture.Common.Repository;
using System.Threading;
using Capture.Common.Model;
using System.Windows;
using Capture.Common;
using System.Windows.Threading;
using Microsoft.Practices.Prism.Regions;
using Capture.Common.Prism;

namespace Capture.Module.Trades.Test
{
    [TestFixture]
    public sealed class LoadTradeViewModelTest
    {
        private IUnityContainer container;

        public static int RepositoryMock_GetTradeList_Count;
        public static string NavigationServiceMock_BaseAddress;
        public static int NavigationServiceMock_NavigationId;

        [TestFixtureSetUp]
        public void PrepareMockedInterfacesForContainer()
        {
            container = new UnityContainer();
            container.RegisterType<ILoadTradeView, LoadTradeViewMock>();
            container.RegisterType<IRepository, RepositoryMock>();
            container.RegisterType<ILoadTradeViewModel, LoadTradeViewModel>();
            container.RegisterType<IRegionManager, RegionManagerMock>();

            Capture.Common.Prism.PrismExtensions.ServiceFactory = p => new NavigationServiceMock(p);

            RepositoryMock_GetTradeList_Count = 0;
        }

        [Test]
        public void T001_Should_Implement_ILoadTradeViewModel()
        {
            // arrange, act
            ILoadTradeViewModel i = this.container.Resolve<ILoadTradeViewModel>();

            // assert
            Assert.IsNotNull(i);
        }

        [Test]
        public void T002_After_Constructed_Should_NOT_Contained_Any_Null_References_Use_Null_Objects_Pattern_Intead()
        {
            // arrange
            ILoadTradeViewModel i = this.container.Resolve<ILoadTradeViewModel>();

            // act, assert

            Assert.IsNotNull(i.CancelTradeLoadCommand);
            Assert.IsNotNull(i.LoadSelectedTradeCommand);
            Assert.IsNotNull(i.NextTradesCommand);
            Assert.IsNotNull(i.PrevTradesCommand);

            Assert.IsNotNull(i.ItemsPerPage);
            Assert.IsNotNull(i.PageList);

            Assert.That(string.IsNullOrEmpty(i.BusyIndicatorText), Is.Not.True);
            Assert.That(i.CurrentPage, Is.EqualTo(0));
            Assert.That(i.IsBusy, Is.EqualTo(false));
            Assert.That(i.CurrentItemsPerPage, Is.EqualTo(10));
            Assert.That(i.ItemsPerPage.Count(), Is.EqualTo(4)); // 10, 25, 50, 100
            // todo: rethink this
            Assert.That(i.PagesCount, Is.EqualTo(1));           // page 0 - so there is one
        }

        [Test]
        public void T003_After_Constructed_Cancel_Command_Should_Return_True_When_Asked_If_Can_Be_Executed()
        {
            // arrange
            ILoadTradeViewModel i = this.container.Resolve<ILoadTradeViewModel>();
            // act
            var b = i.CancelTradeLoadCommand.CanExecute();
            // assert
            Assert.True(b);
        }

        [Test]
        public void T004_After_Constructed_Navigation_Commands_Prev_And_Next_Should_Return_When_Asked_If_Can_Be_Executed()
        {
            // arrange
            ILoadTradeViewModel i = this.container.Resolve<ILoadTradeViewModel>();
            // act
            var prevResult = i.PrevTradesCommand.CanExecute();
            var nextResult = i.NextTradesCommand.CanExecute();
            // assert
            Assert.False(prevResult);
            Assert.False(nextResult);
        }

        [Test]
        public void T005_After_Constructed_Load_Trade_Should_Return_False_When_Asked_If_Can_Execute()
        {
            // arrange
            ILoadTradeViewModel i = this.container.Resolve<ILoadTradeViewModel>();
            // act
            var b = i.LoadSelectedTradeCommand.CanExecute();
            // assert
            Assert.False(b);
        }

        [Test, RequiresSTA]
        public void T006_When_Current_Page_Is_Smaller_Than_Total_Count_Of_Pages_Next_Trades_Command_Could_Be_Executed()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 100;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act            
            var b = sut.NextTradesCommand.CanExecute();
            // assert
            Assert.True(b);
        }

        [Test, RequiresSTA]
        public void T007_When_Trades_Count_Is_Smaller_Than_Items_To_Display_Navigate_Next_And_Prev_Shouldnt_Be_Allowed()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 5;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }
            sut.CurrentItemsPerPage = 10;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act            
            var nextResult = sut.NextTradesCommand.CanExecute();
            var prevResult = sut.PrevTradesCommand.CanExecute();
            // assert
            Assert.False(nextResult);
            Assert.False(prevResult);
        }

        [Test, RequiresSTA]
        public void T008_When_Current_Page_Is_The_Same_As_Total_Pages_Count_Navigate_Next_Shoouldnt_Be_Allowed()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 15;
            sut.CurrentItemsPerPage = 10;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act            
            sut.NextTradesCommand.Execute();    //first navigation OK
            var b = sut.NextTradesCommand.CanExecute();     // no more navigation should be allowed
            // assert
            Assert.False(b);
        }

        [Test, RequiresSTA]
        public void T009_When_Current_Page_Is_The_First_Page_Navigation_Prev_Shouldnt_Be_Allowed()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 15;
            sut.CurrentItemsPerPage = 10;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act            
            sut.NextTradesCommand.Execute();    //next 2/2
            sut.PrevTradesCommand.Execute();    //prev 1/2
            sut.PrevTradesCommand.Execute();    // 0/2 - not allowed
            var b = sut.PrevTradesCommand.CanExecute();     // no more navigation should be allowed
            // assert
            Assert.False(b);
        }

        [Test]
        public void T010_When_Changing_Items_Per_Page_From_10_To_100_And_Current_Displayed_Trades_Displayed_Collection_Should_Be_Updated_As_Well()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 1500;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }
            sut.CurrentItemsPerPage = 10;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act            
            Assert.That(sut.CurrentItemsPerPage, Is.EqualTo(10));
            Assert.That(sut.TradesList.Count, Is.EqualTo(10));
            sut.CurrentItemsPerPage = 100;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            var count = sut.TradesList.Count;
            // assert
            Assert.That(count, Is.EqualTo(100));
        }

        [Test]
        public void T011_When_Current_Page_Was_Changed_Data_Should_Fetched_From_DB()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }
            sut.CurrentItemsPerPage = 10;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // Act
            sut.CurrentPage = 16;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // assert
            Assert.False(sut.NextTradesCommand.CanExecute());
            Assert.That(sut.TradesList.Count, Is.EqualTo(5));

        }

        [Test]
        public void T012_Load_Trade_Command_Should_Not_Be_Available_When_No_Trade_Is_Selected()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }
            

            // assert
            Assert.False(sut.LoadSelectedTradeCommand.CanExecute());
        }

        [Test]
        public void T013_Load_Trade_Command_Should_Available_When_Trade_Is_Selected()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }


            sut.SelectedTradeIndex = 3;

            // assert
            Assert.True(sut.LoadSelectedTradeCommand.CanExecute());
        }

        [Test]
        public void T014_After_Page_Was_Changed_Selection_Should_Be_Removed_From_Data_Grid_And_Selected_Index_Should_Have_A_Value_Of_Minus_One()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            sut.SelectedTradeIndex = 3;

            // act
            Assert.True(sut.LoadSelectedTradeCommand.CanExecute());
            sut.CurrentPage = 3;
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // assert
            Assert.False(sut.LoadSelectedTradeCommand.CanExecute());
            Assert.That(sut.SelectedTradeIndex, Is.EqualTo(-1));
        }

        [Test]
        public void T015_When_Trade_Is_Selected_And_Load_Command_Is_Executed_Navigated_Uri_Should_Container_Selected_Trade_ID_For_The_Next_View_To_Use()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            sut.SelectedTradeIndex = 3;

            // act
            Assert.True(sut.LoadSelectedTradeCommand.CanExecute());
            sut.LoadSelectedTradeCommand.Execute();

            //
            var expectedUri = typeof(IEditTradeView).FullName + "?TradeId=0";
            var expectedRegion = RegionNames.CenterFillRegion;

            // asserts
            Assert.That(LoadTradeViewModelTest.NavigationServiceMock_BaseAddress, Is.EqualTo(typeof(IEditTradeView).FullName));
            Assert.That(LoadTradeViewModelTest.NavigationServiceMock_NavigationId, Is.EqualTo(0));
        }

        [Test]
        public void T016_When_Cancel_Button_Is_Pressed_Load_Trade_Should_Reqest_A_Navigation_With_TradeId_Set_To_Minus_One_And_Selected_View_Should_Be_The_Empty_One()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            sut.SelectedTradeIndex = 3;

            // act
            Assert.True(sut.CancelTradeLoadCommand.CanExecute());
            sut.CancelTradeLoadCommand.Execute();

            //
            var expectedUri = typeof(IEmptyView).FullName + "?TradeId=-1";
            var expectedRegion = RegionNames.CenterFillRegion;

            // asserts
            Assert.That(LoadTradeViewModelTest.NavigationServiceMock_BaseAddress, Is.EqualTo(typeof(IEmptyView).FullName));
            Assert.That(LoadTradeViewModelTest.NavigationServiceMock_NavigationId, Is.EqualTo(-1));

        }

        [Test]
        public void T017_After_Navigated_From_ViewModel_Trade_Collection_Should_Be_Empty()
        {
            // arrange
            var sut = this.container.Resolve<LoadTradeViewModel>();

            LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count = 155;
            sut.OnNavigatedTo(null);
            if (sut.IsBusy == true)
            {
                while (sut.IsBusy)
                {
                    Thread.Sleep(50);
                }
            }

            // act
            Assert.True(sut.TradesList.Count > 0);
            sut.OnNavigatedFrom(null);

            // asserts
            Assert.True(sut.TradesList.Count == 0);
        }

    }


    public sealed class LoadTradeViewMock : ILoadTradeView
    {
        public ViewModelBase ViewModel
        {
            get;
            set;
        }
    }

    public sealed class RepositoryMock : IRepository
    {
        public IEnumerable<ITradeListElemet> GetTradeList(int startIndex, int count, TradeFilter filter = null)
        {
            var r = new List<ITradeListElemet>();
            for (int i = 0; i < LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count; i++)
            {
                r.Add(new TradeListElement());
            }
            return r.Skip(startIndex).Take(count);
            
        }

        public Common.Model.ITradeCapture GetTrade(int tradeId)
        {
            throw new NotImplementedException();
        }

        public void Save(ITradeCapture trade)
        {
            throw new NotImplementedException();
        }

        public int GetTradeCount(TradeFilter filter = null)
        {
            return LoadTradeViewModelTest.RepositoryMock_GetTradeList_Count;
        }


        public ITradeUser LoadUserByName(string username)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class RegionManagerMock : IRegionManager
    {
        public IRegionManager CreateRegionManager()
        {
            throw new NotImplementedException();
        }

        public IRegionCollection Regions
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class NavigationServiceMock : INavigation
    {
        private IRegionManager p;

        public NavigationServiceMock(IRegionManager p)
        {
            this.p = p;
        }

        public IRegionManager AddToRegion(string regionName, object view)
        {
            throw new NotImplementedException();
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Func<object> getContentDelegate)
        {
            throw new NotImplementedException();
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Type viewType)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, string source)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, Uri source)
        {
            //LoadTradeViewModelTest.NavigationServiceMock_RequestedNavigatedTargetRegionName = regionName;
            //LoadTradeViewModelTest.NavigationServiceMock_ReqestedNavigatedTargetUri = source.OriginalString;
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, string source, Action<NavigationResult> navigationCallback)
        {
            throw new NotImplementedException();
        }

        public void RequestNavigate(string regionName, Uri source, Action<NavigationResult> navigationCallback)
        {
            throw new NotImplementedException();
        }


        public void RequestNavigateWithTradeIdAsParam(string baseAddress, int id = -1)
        {
            LoadTradeViewModelTest.NavigationServiceMock_BaseAddress = baseAddress;
            LoadTradeViewModelTest.NavigationServiceMock_NavigationId = id;
        }
    }
}
