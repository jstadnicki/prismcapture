﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using Capture.Module.Trades.MVVM;
using Capture.Common.MVVM;
using Capture.Common.Repository;
using System.Threading;
using Capture.Common.Model;
using System.Windows;
using Capture.Common;
using System.Windows.Threading;
using Microsoft.Practices.Prism.Regions;
using Capture.Common.Prism;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Events;
using Capture.Module.Trades.Messages;
using Capture.Common.Messages;

namespace Capture.Module.Trades.Test
{
    [TestFixture, RequiresSTA]
    public sealed class CommandsTradeViewModelTest
    {
        UnityContainer container;

        [SetUp]
        public void SetupTests()
        {
            if (this.container != null)
            {
                this.container.Dispose();
                this.container = null;
            }
            this.container = new UnityContainer();
            this.container.RegisterType<ICommandsTradeViewModel, CommandsTradeViewModel>();
            this.container.RegisterType<IRegionManager, RegionManager>();
            this.container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());
        }




        [Test]
        public void T000_CommandsVM_Should_Implement_Interface()
        {
            // arrange
            // act
            var sut = this.container.Resolve<ICommandsTradeViewModel>();

            // assert
            Assert.NotNull(sut);
        }

        [Test]
        public void T001_Save_Command_Execute_Should_Broadcast_Appcalition_Command_With_Save_Request()
        {
            // arrange
            var sut = this.container.Resolve<ICommandsTradeViewModel>();
            var evMsg = this.container.Resolve<IEventAggregator>();
            bool received = false;

            evMsg.GetEvent<TradeCaptureApplicationCommands>().Subscribe(a =>
                {
                    if (a.CommandType == ApplicationCommandsType.SaveTrade)
                    {
                        received = true;
                    }
                });
            // act

            sut.SaveTradeCommand.Execute();

            // assert
            Assert.True(received);
        }
    }
}