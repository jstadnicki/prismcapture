﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;

namespace Capture.Module.Trades.Test
{
    sealed class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            NUnit.Gui.AppEntry.Main(new string[] {
                Assembly.GetExecutingAssembly().Location });
        }
    }
}
