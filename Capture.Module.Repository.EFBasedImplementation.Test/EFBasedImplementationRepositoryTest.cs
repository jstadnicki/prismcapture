﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Capture.Common.Repository;
using Microsoft.Practices.Unity;
using Capture.Model;
using Capture.Common.Model;

namespace Capture.Module.Repository.EFBasedImplementation.Test
{
    [TestFixture]
    public sealed class EFBasedImplementationRepositoryTest
    {
        UnityContainer container = new UnityContainer();

        [TestFixtureSetUp]
        public void SetupFixture()
        {
            this.container.RegisterType<IRepository, Repository>();
            this.container.RegisterType<ITradeCapture, TradeCapture>();
        }

        [Test]
        public void T001_Repository_Should_Return_Zero_Result_From_Empty_Database()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();

            // act
            var result = r.GetTradeList();

            // act
            Assert.That(result.Count(), Is.EqualTo(0));
        }

        [Test]
        public void T002_Repository_Should_Return_All_Trades_That_Are_In_Database_Using_ITradeListElement_Interface()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(10);

            // act
            var result = r.GetTradeList();

            // assert
            Assert.That(result.Count(), Is.EqualTo(10));
        }

        [Test]
        public void T003_Repository_Should_Fetach_All_Histories_Attached_To_A_Trade()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(1);
            this.GenerateRandomHistory(10);

            // act
            var result = r.GetTradeList();

            // assert
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.First().Histories.Count(), Is.EqualTo(10));
        }

        [Test]
        public void T004_Repository_When_TradeHistory_Was_Changed_It_Should_Be_Added_To_Trade_History_Collection_And_Saved_In_DB()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(1);
            this.GenerateRandomHistory(10);

            var tradeId = -1;

            using (var e = new TCEntities())
            {
                tradeId = e.Trades.First().Id;
            }

            var trade = r.GetTrade(tradeId);

            int newUnitsCount = 654;

            // act
            trade.Current.Units = newUnitsCount;

            // assert
            r.Save(trade);
        }

        [Test]
        public void T005_Repository_When_Provided_Valid_Trade_Id_Should_Return_Trade_With_Full_History()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(1);
            this.GenerateRandomHistory(10);

            Trade expected;
            int expectedHistoryCount = -1;
            using (var e = new TCEntities())
            {
                expected = e.Trades.First();
                expectedHistoryCount = expected.Histories.Count;
            }

            // act
            var trade = r.GetTrade(expected.Id);

            // assert
            Assert.NotNull(trade);
            Assert.That(trade.TradeId, Is.EqualTo(expected.Id));
            Assert.That(trade.AuthorId, Is.EqualTo(expected.Author));
            Assert.That(trade.ViewId, Is.EqualTo(expected.View));
            Assert.That(trade.Created, Is.EqualTo(expected.Created));
            Assert.That(trade.Histories.Count(), Is.EqualTo(expectedHistoryCount));
        }

        [Test]
        public void T006_Repository_When_No_Changes_Were_Introduced_To_History_Calling_Save_Should_Not_Add_New_History_To_TradeCapture()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(1);
            this.GenerateRandomHistory(10);

            int expectedHistoryCount = -1;
            int expectedId = -1;
            using (var e = new TCEntities())
            {
                expectedHistoryCount = e.Trades.First().Histories.Count;
                expectedId = e.Trades.First().Id;
            }

            var trade = r.GetTrade(expectedId);

            // act

            r.Save(trade);
            r.Save(trade);
            r.Save(trade);
            r.Save(trade);


            // assert
            Assert.That(trade.Histories.Count(), Is.EqualTo(expectedHistoryCount));
        }

        [Test]
        public void T007_Repository_When_TradeHistory_Was_Changed_It_Should_Be_Added_To_Trade_History_Collection_And_All_Fields_From_It_Should_Be_Saved_In_DB()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(1);
            this.GenerateRandomHistory(10);

            var tradeId = -1;

            using (var e = new TCEntities())
            {
                tradeId = e.Trades.First().Id;
            }

            var trade = r.GetTrade(tradeId);

            int UnitsT = 1000000;
            int AnnualizationT = 1000001;
            int CalculatorMaxStringT = 1000002;
            int CalculatorMinStrikeT = 1000003;
            int CalculatorStrikeT = 1000004;
            int CalculatorStrikeUsedT = 1000005;
            int CalculatorVolOfVolT = 1000006;
            int CalculatorVolSpreadT = 1000007;
            int ExpectedNT = 1000008;
            int FixFinalSettingT = 1000009;
            int FixFirstSettingT = 1000010;
            int IdT = 1000011;
            int NominalT = 1000012;
            int PaymentLagT = 10000013;
            int PeriodsT = 1000014;
            int ReferenceVolatilityT = 1000015;
            int RollDayT = 1000016;
            int StrikeT = 1000017;
            int TradeIdT = 1000018;
            int VegaAmountT = 1000019;
            int VolumeCaptureT = 1000020;

            string AuthorT = "AAAAAAAAAAAAA";
            string BitT = "BBBBBBBBBBBBB";
            string CalculatorNameT = "CCCCCCCCCCCCC";
            string ContractT = "DDDDDDDDDDDDD";
            string DetailsAgentT = "EEEEEEEEEEEEE";
            string DetailsBookT = "FFFFFFFFFFFFF";
            string DetailsBrokerCodeT = "GGGGGGGGGGGGG";
            string DetailsCounterBookT = "HHHHHHHHHHHHH";
            string DetailsCptyCodeT = "IIIIIIIIIIIII";
            string DetailsMarketerT = "JJJJJJJJJJJJJ";
            string DetailsTraderT = "KKKKKKKKKKKKK";
            string EconomicCommentsT = "LLLLLLLLLLLLL";
            string ExpiryT = "MMMMMMMMMMMMM";
            string ExpiryDataCallendarT = "NNNNNNNNNN";
            string FrequencyT = "O";
            string IndexNameT = "PPPPPPPPPPPPP";
            string NationalCurrencyT = "RRRRRRRRRRRRR";
            string NonDeliverableT = "SSSSSSSSSSSSS";
            string PaymentCallendarT = "TTTTTTTTTTTTT";
            string ObsoleteConditionT = "UUUUUUUUUUUUU";
            string PaymentCurrencyT = "WWWWWWWWWWWWW";
            string ProductTypeT = "XXXXXXXXXXXXX";
            string RollingMCDT = "YYYYYYYYYYYYY";
            string StructureTypeT = "ZZZZZZZZZZZZZ";
            string SwapTypeT = "QQQQQQQQQQQQQ";


            DateTime CreatedT = DateTime.Now.AddDays(1);
            DateTime EffectiveDateT = DateTime.Now.AddDays(2);
            DateTime ExpiryDateT = DateTime.Now.AddDays(3);
            DateTime ExpiryOffsetT = DateTime.Now.AddDays(4);
            DateTime PaymentDateT = DateTime.Now.AddDays(5);
            DateTime RegularDatesFirstT = DateTime.Now.AddDays(6);
            DateTime RegulatDatesLastT = DateTime.Now.AddDays(7);
            DateTime SubstractDaysT = DateTime.Now.AddDays(8);


            // act
            trade.Current.Author = AuthorT;
            trade.Current.Created = CreatedT;
            trade.Current.Annualization = AnnualizationT;
            trade.Current.AnnualizationEnabled = true;
            trade.Current.Bit = BitT;
            trade.Current.CalculatorDelta = true;
            trade.Current.CalculatorFWD = true;
            trade.Current.CalculatorIRCorrection = true;
            trade.Current.CalculatorMaxString = CalculatorMaxStringT;
            trade.Current.CalculatorMinStrike = CalculatorMinStrikeT;
            trade.Current.CalculatorName = CalculatorNameT;
            trade.Current.CalculatorStrike = CalculatorStrikeT;
            trade.Current.CalculatorStrikeUsed = CalculatorStrikeUsedT;
            trade.Current.CalculatorVolOfVol = CalculatorVolOfVolT;
            trade.Current.CalculatorVolSpread = CalculatorVolSpreadT;
            trade.Current.Composite = true;
            trade.Current.Contract = ContractT;
            trade.Current.DetailsAgent = DetailsAgentT;
            trade.Current.DetailsBook = DetailsBookT;
            trade.Current.DetailsBrokerCode = DetailsBrokerCodeT;
            trade.Current.DetailsCounterBook = DetailsCounterBookT;
            trade.Current.DetailsCptyCode = DetailsCptyCodeT;
            trade.Current.DetailsMarketer = DetailsMarketerT;
            trade.Current.DetailsTrader = DetailsTraderT;
            trade.Current.EconomicComments = EconomicCommentsT;
            trade.Current.EffectiveDate = EffectiveDateT;
            trade.Current.ExpectedN = ExpectedNT;
            trade.Current.ExpectedNEnabled = true;
            trade.Current.Expiry = ExpiryT;
            trade.Current.ExpiryDataCallendar = ExpiryDataCallendarT;
            trade.Current.ExpiryDate = ExpiryDateT;
            trade.Current.ExpiryOffset = ExpiryOffsetT;
            trade.Current.FixFinalParamEnabled = true;
            trade.Current.FixFinalSetting = FixFinalSettingT;
            trade.Current.FixFirstParamEnabled = true;
            trade.Current.FixFirstSetting = FixFirstSettingT;
            trade.Current.Frequency = FrequencyT;
            trade.Current.Id = IdT;
            trade.Current.IndexName = IndexNameT;
            trade.Current.InitialiExchange = true;
            trade.Current.ManageRisk = true;
            trade.Current.MultipleExchange = true;
            trade.Current.NationalCurrency = NationalCurrencyT;
            trade.Current.Nominal = NominalT;
            trade.Current.NominalMarketDisrupt = true;
            trade.Current.NonDeliverable = NonDeliverableT;
            trade.Current.NonDeliverableEnabled = true;
            trade.Current.ObsoleteCondition = ObsoleteConditionT;
            trade.Current.PaymentCallendar = PaymentCallendarT;
            trade.Current.PaymentCurrency = PaymentCurrencyT;
            trade.Current.PaymentDate = PaymentDateT;
            trade.Current.PaymentLag = PaymentLagT;
            trade.Current.Periods = PeriodsT;
            trade.Current.ProductType = ProductTypeT;
            trade.Current.ReferenceVolatility = ReferenceVolatilityT;
            trade.Current.RegularDatesFirst = RegularDatesFirstT;
            trade.Current.RegulatDatesLast = RegulatDatesLastT;
            trade.Current.RollDay = RollDayT;
            trade.Current.RollingMCD = RollingMCDT;
            trade.Current.Selling = true;
            trade.Current.Strike = StrikeT;
            trade.Current.StructureType = StructureTypeT;
            trade.Current.SubstractDays = SubstractDaysT;
            trade.Current.SwapType = SwapTypeT;
            trade.Current.TradeId = TradeIdT;
            trade.Current.Units = UnitsT;
            trade.Current.VegaAmount = VegaAmountT;
            trade.Current.VolumeCapture = VolumeCaptureT;
            trade.Current.VolumeCaptureEnabled = true;

            r.Save(trade);

            // assert

            using (var db = new TCEntities())
            {
                var tradeTestObject = db.Trades.Where(t => t.Id == tradeId).First();
                var historyTestObject = db.Histories.Where(h => h.TradeId == tradeId)
                    .ToList()
                    .OrderBy(h => h.Id)
                    .Reverse()
                    .First();


                //TODO should test it or not?
                //Assert.Ignore(historyTestObject.Author == AuthorT);
                //Assert.True(historyTestObject.Created == CreatedT);
                //Assert.True(historyTestObject.TradeId == TradeIdT);

                Assert.True(historyTestObject.VolumeCaptureEnabled == true);
                Assert.True(historyTestObject.Units == UnitsT);
                Assert.True(historyTestObject.Annualization == AnnualizationT);
                Assert.True(historyTestObject.AnnualizationEnabled == true);
                Assert.True(historyTestObject.Bit == BitT);
                Assert.True(historyTestObject.CalculatorDelta == true);
                Assert.True(historyTestObject.CalculatorFWD == true);
                Assert.True(historyTestObject.CalculatorIRCorrection == true);
                Assert.True(historyTestObject.CalculatorMaxString == CalculatorMaxStringT);
                Assert.True(historyTestObject.CalculatorMinStrike == CalculatorMinStrikeT);
                Assert.True(historyTestObject.CalculatorName == CalculatorNameT);
                Assert.True(historyTestObject.CalculatorStrike == CalculatorStrikeT);
                Assert.True(historyTestObject.CalculatorStrikeUsed == CalculatorStrikeUsedT);
                Assert.True(historyTestObject.CalculatorVolOfVol == CalculatorVolOfVolT);
                Assert.True(historyTestObject.CalculatorVolSpread == CalculatorVolSpreadT);
                Assert.True(historyTestObject.Composite == true);
                Assert.True(historyTestObject.Contract == ContractT);
                Assert.True(historyTestObject.DetailsAgent == DetailsAgentT);
                Assert.True(historyTestObject.DetailsBook == DetailsBookT);
                Assert.True(historyTestObject.DetailsBrokerCode == DetailsBrokerCodeT);
                Assert.True(historyTestObject.DetailsCounterBook == DetailsCounterBookT);
                Assert.True(historyTestObject.DetailsCptyCode == DetailsCptyCodeT);
                Assert.True(historyTestObject.DetailsMarketer == DetailsMarketerT);
                Assert.True(historyTestObject.DetailsTrader == DetailsTraderT);
                Assert.True(historyTestObject.EconomicComments == EconomicCommentsT);
                Assert.True(historyTestObject.EffectiveDate == EffectiveDateT.Date);
                Assert.True(historyTestObject.ExpectedN == ExpectedNT);
                Assert.True(historyTestObject.ExpectedNEnabled == true);
                Assert.True(historyTestObject.Expiry == ExpiryT);
                Assert.True(historyTestObject.ExpiryDataCallendar == ExpiryDataCallendarT);
                Assert.True(historyTestObject.ExpiryDate == ExpiryDateT.Date);
                Assert.True(historyTestObject.ExpiryOffset == ExpiryOffsetT);
                Assert.True(historyTestObject.FixFinalParamEnabled == true);
                Assert.True(historyTestObject.FixFinalSetting == FixFinalSettingT);
                Assert.True(historyTestObject.FixFirstParamEnabled == true);
                Assert.True(historyTestObject.FixFirstSetting == FixFirstSettingT);
                Assert.True(historyTestObject.Frequency == FrequencyT);
                //Assert.True(historyTestObject.Id == IdT);
                Assert.True(historyTestObject.IndexName == IndexNameT);
                Assert.True(historyTestObject.InitialiExchange == true);
                Assert.True(historyTestObject.ManageRisk == true);
                Assert.True(historyTestObject.MultipleExchange == true);
                Assert.True(historyTestObject.NationalCurrency == NationalCurrencyT);
                Assert.True(historyTestObject.Nominal == NominalT);
                Assert.True(historyTestObject.NominalMarketDisrupt == true);
                Assert.True(historyTestObject.NonDeliverable == NonDeliverableT);
                Assert.True(historyTestObject.NonDeliverableEnabled == true);
                Assert.True(historyTestObject.ObsoleteCondition == ObsoleteConditionT);
                Assert.True(historyTestObject.PaymentCallendar == PaymentCallendarT);
                Assert.True(historyTestObject.PaymentCurrency == PaymentCurrencyT);
                Assert.True(historyTestObject.PaymentDate == PaymentDateT.Date);
                Assert.True(historyTestObject.PaymentLag == PaymentLagT);
                Assert.True(historyTestObject.Periods == PeriodsT);
                Assert.True(historyTestObject.ProductType == ProductTypeT);
                Assert.True(historyTestObject.ReferenceVolatility == ReferenceVolatilityT);
                Assert.True(historyTestObject.RegularDatesFirst == RegularDatesFirstT.Date);
                Assert.True(historyTestObject.RegulatDatesLast == RegulatDatesLastT.Date);
                Assert.True(historyTestObject.RollDay == RollDayT);
                Assert.True(historyTestObject.RollingMCD == RollingMCDT);
                Assert.True(historyTestObject.Selling == true);
                Assert.True(historyTestObject.Strike == StrikeT);
                Assert.True(historyTestObject.StructureType == StructureTypeT);
                Assert.True(historyTestObject.SubstractDays == SubstractDaysT);
                Assert.True(historyTestObject.SwapType == SwapTypeT);
                Assert.True(historyTestObject.Units == UnitsT);
                Assert.True(historyTestObject.VegaAmount == VegaAmountT);
                Assert.True(historyTestObject.VolumeCapture == VolumeCaptureT);
                Assert.True(historyTestObject.VolumeCaptureEnabled == true);
            }

        }

        [Test]
        public void T008_When_Username_With_Given_Name_Does_Not_Exists_Should_Return_Null()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();

            // act
            var u = r.LoadUserByName("alalalalalasdasldkajsd");

            // assert
            Assert.Null(u);
        }

        [Test]
        public void T009_When_Username_With_Given_Name_Does_Exists_Should_Return_That_User_With_All_Information_Updated()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();

            this.RemoveAllDataFromDatabase();
            this.GenerateRandomUsers();

            // act
            var result = r.LoadUserByName("TestUser_0");
            var expected = new TradeUser();

            using (var db = new TCEntities())
            {
                var user = db.Users.Where(u => u.Username == "TestUser_0").Single();
                expected.HashPassword = user.Userpassword.Trim();
                expected.Id = user.Id;
                expected.Username = user.Username.Trim();
                expected.Account = (AccountType)user.Account;
                expected.FirstName = user.FirstName.Trim();
                expected.LastName = user.LastName.Trim();
                expected.UserPicture = user.Image;
            }

            // assert
            Assert.NotNull(result);
            Assert.That(expected.Username, Is.EqualTo(result.Username));
            Assert.That(expected.Id, Is.EqualTo(result.Id));
            Assert.That(expected.HashPassword, Is.EqualTo(result.HashPassword));
            Assert.That(expected.Account, Is.EqualTo(result.Account));
            Assert.That(expected.FirstName, Is.EqualTo(result.FirstName));
            Assert.That(expected.LastName, Is.EqualTo(result.LastName));
            Assert.That(expected.UserPicture, Is.EqualTo(result.UserPicture));

        }

        [Test]
        public void T010_When_Filter_Only_With_New_Status_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_TradeListItems()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Id== t.Histories.Max(d => d.Id)
                           select new { t, h };

                var rst2 = from t in rst1
                           where t.h.Status == (int)TradeStatus.NewTrade
                           select t;

                expected = rst2.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddFilterSpecification(TradeStatus.NewTrade);

            // act
            var result = r.GetTradeList(0, int.MaxValue, tradeFilter);

            // assert
            Assert.AreEqual(expected, result.Count());
        }

        [Test]
        public void T011_When_Filter_For_Specified_User_Id_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_TradeListItems()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Created == t.Histories.Max(d => d.Created)
                           select new { t, h };

                var rst2 = from t in rst1
                           where t.t.Author == 2
                           select t;

                expected = rst2.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddAuthorSpecification(2);

            // act
            var result = r.GetTradeList(0, int.MaxValue, tradeFilter);

            // assert
            Assert.AreEqual(expected, result.Count());
        }

        [Test]
        public void T012_When_Filter_For_Specified_Creation_Date_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_TradeListItems()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            var dateToTest = DateTime.Now.AddDays(-7);

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Id == t.Histories.Max(d => d.Id)
                           select new { t, h };

                var rst2 = from t in rst1                           // after date
                           where t.h.Created >= dateToTest
                           select t;

                var rst3 = from t in rst2                           // specific user
                           where t.h.Author == 2
                           select t;

                expected = rst3.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddModificationAuthorSpecification(2)
                .AddLastModificationAfter(dateToTest);

            // act
            var result = r.GetTradeList(0, int.MaxValue, tradeFilter);

            // assert
            Assert.AreEqual(expected, result.Count());
        }

        [Test]
        public void T013_When_Filter_Only_With_New_Status_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_Count_Of_Trades()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Id == t.Histories.Max(d => d.Id)
                           select new { t, h };

                var rst2 = from t in rst1
                           where t.h.Status == (int)TradeStatus.NewTrade
                           select t;

                expected = rst2.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddFilterSpecification(TradeStatus.NewTrade);

            // act
            var result = r.GetTradeCount(tradeFilter);

            // assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void T014_When_Filter_For_Specified_User_Id_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_Count_Of_Trades()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Created == t.Histories.Max(d => d.Created)
                           select new { t, h };

                var rst2 = from t in rst1
                           where t.t.Author == 2
                           select t;

                expected = rst2.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddAuthorSpecification(2);

            // act
            var result = r.GetTradeCount(tradeFilter);

            // assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void T015_When_Filter_For_Specified_Creation_Date_Is_Set_Should_Take_It_Under_Consideration_When_Displaying_Count_Of_Trades()
        {
            // arrange
            IRepository r = this.container.Resolve<IRepository>();
            this.RemoveAllDataFromDatabase();
            this.GenerateRandomTrades(50);
            this.GenerateRandomHistory(1000);

            int expected = 0;

            var dateToTest = DateTime.Now.AddDays(-7);

            using (var db = new TCEntities())
            {
                var rst1 = from t in db.Trades
                           join h in db.Histories on t.Id equals h.TradeId
                           where h.Created == t.Histories.Max(d => d.Created)
                           select new { t, h };

                var rst2 = from t in rst1                           // after date
                           where t.h.Created >= dateToTest
                           select t;

                var rst3 = from t in rst2                           // specific user
                           where t.h.Author == 2
                           select t;

                expected = rst3.Count();
            }

            var tradeFilter = new TradeFilter()
                .AddModificationAuthorSpecification(2)
                .AddLastModificationAfter(dateToTest);

            // act
            var result = r.GetTradeCount( tradeFilter);

            // assert
            Assert.AreEqual(expected, result);
        }

        private void GenerateRandomHistory(int count)
        {
            this.GenerateStatuses();
            var random = new Random(DateTime.Now.Millisecond);

            using (var e = new TCEntities())
            {
                var users = e.Users.ToList();
                var trades = e.Trades.ToList();
                var statuses = e.Statuses.ToList();

                for (int i = 0; i < count; i++)
                {

                    var h = History.CreateHistory(-1,
                        users.ElementAt(random.Next(users.Count)).Id,
                        DateTime.Now.AddDays(random.Next(-2000, 0)),
                        trades.ElementAt(random.Next(trades.Count)).Id,
                        statuses.ElementAt(random.Next(statuses.Count)).Id);

                    e.Histories.AddObject(h);
                }

                e.SaveChanges();
            }
        }

        private void GenerateStatuses()
        {
            using (var db = new TCEntities())
            {
                if (db.Statuses.Count() == 0)
                {
                    var s1 = Status.CreateStatus(-1, "NewTrade");
                    var s2 = Status.CreateStatus(-1, "UnderReview");
                    var s3 = Status.CreateStatus(-1, "Proposed");
                    var s4 = Status.CreateStatus(-1, "Accepted");
                    var s5 = Status.CreateStatus(-1, "Rejected");
                    var s6 = Status.CreateStatus(-1, "Deleted");
                    var s7 = Status.CreateStatus(-1, "Executed");

                    db.Statuses.AddObject(s1);
                    db.Statuses.AddObject(s2);
                    db.Statuses.AddObject(s3);
                    db.Statuses.AddObject(s4);
                    db.Statuses.AddObject(s5);
                    db.Statuses.AddObject(s6);
                    db.Statuses.AddObject(s7);

                    db.SaveChanges();
                }
            }
        }

        private void RemoveAllDataFromDatabase()
        {
            using (var e = new Capture.Model.TCEntities())
            {
                foreach (var item in e.Histories)
                {
                    e.Histories.DeleteObject(item);
                }

                foreach (var item in e.Trades)
                {
                    e.Trades.DeleteObject(item);
                }

                foreach (var item in e.ViewSchemas)
                {
                    e.ViewSchemas.DeleteObject(item);
                }

                e.SaveChanges();
            }
        }

        private void GenerateAccounts()
        {
            using (var db = new TCEntities())
            {
                if (db.Accounts.Count() == 0)
                {
                    var a = Account.CreateAccount(-1, "Administrator", (int)AccountType.Administrator);

                    var j = Account.CreateAccount(-1, "Junior", (int)AccountType.Junior);
                    var r = Account.CreateAccount(-1, "Regular", (int)AccountType.Regular);
                    var s = Account.CreateAccount(-1, "Senior", (int)AccountType.Senior);
                    var m = Account.CreateAccount(-1, "Manager", (int)AccountType.Manager);


                    db.Accounts.AddObject(a);
                    db.Accounts.AddObject(j);
                    db.Accounts.AddObject(r);
                    db.Accounts.AddObject(s);
                    db.Accounts.AddObject(m);

                    db.SaveChanges();
                }
            }
        }

        private void GenerateRandomTrades(int count)
        {
            this.GenerateAccounts();
            this.GenerateRandomUsers();
            this.GenerateRandomSchemas(10);

            Random random = new Random(DateTime.Now.Millisecond);


            using (var e = new TCEntities())
            {
                var usersList = e.Users.Select(u => u.Id).ToList();
                var viewList = e.ViewSchemas.Select(v => v.Id).ToList();


                for (int i = 0; i < count; i++)
                {
                    e.Trades.AddObject(Trade.CreateTrade(-1,
                        usersList.ElementAt(random.Next(usersList.Count)),
                        DateTime.Now,
                        viewList.ElementAt(random.Next(viewList.Count))));

                }

                e.SaveChanges();
            }
        }

        private void GenerateRandomSchemas(int count)
        {
            string xmlschema = "<?xml version=\"1.0\"?><root/>";

            using (var e = new TCEntities())
            {
                for (int i = 0; i < count; i++)
                {
                    e.ViewSchemas.AddObject(ViewSchema.CreateViewSchema(-1, xmlschema));
                }
                e.SaveChanges();

            }
        }

        private void GenerateRandomUsers()
        {
            using (var db = new TCEntities())
            {
                if (db.Users.Count() == 0)
                {
                    Random r = new Random(DateTime.Now.Millisecond);

                    for (int i = 0; i < 15; i++)
                    {
                        db.Users.AddObject(
                            User.CreateUser(
                            -1,
                            "TestUser_" + i,
                            "TestPassword_" + i,
                            i % 2 == 0,
                            r.Next(5) + 1,
                            "TestFirstname_" + i,
                            "TestLastname_" + i));
                    }

                    db.SaveChanges();
                }
            }
        }

    }
}
