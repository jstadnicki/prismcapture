﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Capture.Common.Repository;
using Capture.Model;
using System.Data.Objects;
using Capture.Common.Model;
using System.Reflection;

namespace Capture.Module.Repository.EFBasedImplementation
{
    sealed class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            NUnit.Gui.AppEntry.Main(new string[] {
                Assembly.GetExecutingAssembly().Location });
        }
    }
}
