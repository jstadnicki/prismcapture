﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using Capture.Common.Messages;
using Capture.Common.Model;
using Microsoft.Practices.Unity;

namespace Capture.Shell
{
    sealed class ShellViewModel : IShellViewModel
    {
        private IEventAggregator eventAggregator;
        private IUnityContainer container;

        public ShellViewModel(IEventAggregator eventAggregator, IUnityContainer container)
        {
            this.eventAggregator = eventAggregator;
            this.container = container;

            // Close Application
            this.eventAggregator.GetEvent<TradeCaptureApplicationCommands>()
                .Subscribe(new Action<TradeApplicationCommand>(cmd =>
                    {
                        if (cmd.CommandType == ApplicationCommandsType.CloseApplication)
                        {
                            App.Current.Shutdown();
                        }
                    }));

            // Login user
            this.eventAggregator.GetEvent<LoginCaptureUser>()
                .Subscribe(new Action<ITradeUser>(cmd =>
                    {
                        this.container.RegisterInstance<ITradeUser>(cmd);
                        this.eventAggregator
                            .GetEvent<TradeCaptureApplicationCommands>()
                            .Publish(new TradeApplicationCommand { 
                                CommandType = ApplicationCommandsType.InitializeApplication });
                    }));

        }

    }
}
