﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Events;
using Capture.Common.Messages;
using Microsoft.Practices.Prism.Regions;

namespace Capture.Shell
{
    sealed class Bootstrapper : UnityBootstrapper
    {
        IEventAggregator eventAggregator;
        IModuleManager moduleManager;
        IRegionManager regionManager;
        protected override DependencyObject CreateShell()
        {
            Container.RegisterType<IShellViewModel, ShellViewModel>();
            return Container.Resolve<ShellView>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            App.Current.MainWindow = (Window)this.Shell;
            App.Current.MainWindow.Show();

            this.eventAggregator = this.Container.Resolve<IEventAggregator>();
            this.moduleManager = this.Container.Resolve<IModuleManager>();
            this.regionManager = this.Container.Resolve<IRegionManager>();

            this.RegisterToApplicationMessages();
        }

        private void RegisterToApplicationMessages()
        {
            this.eventAggregator.GetEvent<TradeCaptureApplicationCommands>()
                .Subscribe(new Action<TradeApplicationCommand>(cmd =>
                {
                    if (cmd.CommandType == ApplicationCommandsType.InitializeApplication)
                    {
                        foreach (var r in this.regionManager.Regions)
                        {
                            foreach (var v in r.Views)
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() => r.Remove(v)), null);
                            }
                        }

                        var list = this.ModuleCatalog.Modules.Where(m => m.State == ModuleState.NotStarted);
                        foreach (var mod in list)
                        {
                            Application.Current.Dispatcher.Invoke(new Action(() =>
                            this.moduleManager.LoadModule(mod.ModuleName)), null);
                        }
                    }
                }));
        }

        protected override void ConfigureModuleCatalog()
        {
            this.RegisterModuleOf(typeof(Capture.Module.Repository.EFBasedImplementation.RepositoryModule));
            this.RegisterModuleOf(typeof(Module.AuthorizationAuthentication.AAModule));
            this.RegisterModuleOf(typeof(Module.WelcomeScreen.WelcomeScreenModule), InitializationMode.OnDemand);
            this.RegisterModuleOf(typeof(Module.Trades.TradesModule), InitializationMode.OnDemand);
        }

        private void RegisterModuleOf(Type module, InitializationMode mode = InitializationMode.WhenAvailable)
        {
            ModuleCatalog.AddModule(new ModuleInfo()
            {
                ModuleName = module.Name,
                ModuleType = module.AssemblyQualifiedName,
                InitializationMode = mode,
            });
        }

    }
}
