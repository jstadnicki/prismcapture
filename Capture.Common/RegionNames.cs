﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capture.Common
{
    public static class RegionNames
    {
        public static string MenuBarRegion = "MenuBarRegionName";
        public static string StatusBarRegion = "StatusBarRegionName";
        public static string LeftBarRegion = "LeftBarRegionName";
        public static string RightBarRegion = "RightBarRegionName";
        public static string CenterFillRegion = "CenterFillRegionName";
    }
}
