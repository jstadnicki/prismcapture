﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capture.Common.Model
{
    public sealed class TradeListElement : ITradeListElemet
    {
        public IEnumerable<ITradeHistory> Histories
        {
            get;
            set;
        }

        public int TradeId
        {
            get;
            set;
        }

        public DateTime Crated
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("TradeId {0}", TradeId);
            sb.AppendFormat("Author {0}", Author);
            sb.AppendFormat("Crated {0}", this.Crated.ToShortDateString());
            
            return sb.ToString();
        }
    }
}
