﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Controls;

namespace Capture.Common.Model
{
    public class TradeUser : ITradeUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string HashPassword { get; set; }
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }
        public AccountType Account { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] UserPicture  { get; set; }
    }
}
