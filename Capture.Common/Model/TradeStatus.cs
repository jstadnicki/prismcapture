﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public enum TradeStatus
    {
        NewTrade = 1,
        UnderReview,
        Proposed,
        Accepted,
        Rejected,
        Deleted,
        Executed
    }
}
