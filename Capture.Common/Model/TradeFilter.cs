﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public class TradeFilter 
    {

        public TradeFilter AddFilterSpecification(TradeStatus tradeStatus)
        {
            this.Status = tradeStatus;
            return this;
        }

        public TradeFilter AddAuthorSpecification(int authorId)
        {
            this.AuthorId = authorId;
            return this;
        }

        public TradeFilter AddLastModificationAfter(DateTime date)
        {
            this.LastModificationAfter = date;
            return this;
        }

        public TradeStatus? Status { get; set; }
        public int? AuthorId { get; set; }
        public int? ModificationAuthor { get; set; }
        public DateTime? LastModificationAfter { get; set; }

        public TradeFilter AddModificationAuthorSpecification(int p)
        {
            this.ModificationAuthor = p;
            return this;
        }

    }

}