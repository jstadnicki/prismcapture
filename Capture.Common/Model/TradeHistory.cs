﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public sealed class TradeHistory : ITradeHistory, INotifyPropertyChanged
    {
        public DateTime Created
        {
            get { return this.created; }
            set { this.UpdateField(ref created, value, "Created"); }
        }
        DateTime created;

        public int TradeId
        {
            get { return this.tradeId; }
            set { UpdateField(ref tradeId, value, "TradeId"); }
        }
        int tradeId;

        public string Author
        {
            get { return this.author; }
            set { UpdateField(ref author, value, "Author"); }
        }
        string author;

        public int AuthorId { get; set; }

        public TradeStatus Status
        {
            get { return this.status; }
            set { UpdateField(ref status, value, "Status"); }
        }
        TradeStatus status;

        public int Id
        {
            get { return this.id; }
            set { UpdateField(ref id, value, "Id"); }
        }
        int id;

        public decimal? VegaAmount
        {
            get { return this.vegaAmount; }
            set { UpdateField(ref vegaAmount, value, "VegaAmount"); }
        }
        decimal? vegaAmount;

        public int? Units
        {
            get { return this.units; }
            set { UpdateField(ref units, value, "Units"); }
        }
        int? units;

        public decimal? Strike
        {
            get { return this.strike; }
            set { UpdateField(ref strike, value, "Strike"); }
        }
        decimal? strike;

        public decimal? VolumeCapture
        {
            get { return this.volumeCapture; }
            set { UpdateField(ref volumeCapture, value, "VolumeCapture"); }
        }
        decimal? volumeCapture;

        public decimal? Nominal
        {
            get { return this.nominal; }
            set { UpdateField(ref nominal, value, "Nominal"); }
        }
        decimal? nominal;

        public decimal? ReferenceVolatility
        {
            get { return this.referenceVolatility; }
            set { UpdateField(ref referenceVolatility, value, "ReferenceVolatility"); }
        }
        decimal? referenceVolatility;

        public decimal? CalculatorMinStrike
        {
            get { return this.calculatorMinStrike; }
            set { UpdateField(ref calculatorMinStrike, value, "CalculatorMinStrike"); }
        }
        decimal? calculatorMinStrike;

        public decimal? CalculatorMaxString
        {
            get { return this.calculatorMaxString; }
            set { UpdateField(ref calculatorMaxString, value, "CalculatorMaxString"); }
        }
        decimal? calculatorMaxString;

        public bool? Selling
        {
            get { return this.selling; }
            set { UpdateField(ref selling, value, "Selling"); }
        }
        bool? selling;

        public bool? AnnualizationEnabled
        {
            get { return this.annualizationEnabled; }
            set { UpdateField(ref annualizationEnabled, value, "AnnualizationEnabled"); }
        }
        bool? annualizationEnabled;

        public bool? FixFinalParamEnabled
        {
            get { return this.fixFinalParamEnabled; }
            set { UpdateField(ref fixFinalParamEnabled, value, "FixFinalParamEnabled"); }
        }
        bool? fixFinalParamEnabled;

        public bool? ExpectedNEnabled
        {
            get { return this.expectedNEnabled; }
            set { UpdateField(ref expectedNEnabled, value, "ExpectedNEnabled"); }
        }
        bool? expectedNEnabled;

        public bool? VolumeCaptureEnabled
        {
            get { return this.volumeCaptureEnabled; }
            set { UpdateField(ref volumeCaptureEnabled, value, "VolumeCaptureEnabled"); }
        }
        bool? volumeCaptureEnabled;

        public bool? FixFirstParamEnabled
        {
            get { return this.fixFirstParamEnabled; }
            set { UpdateField(ref fixFirstParamEnabled, value, "FixFirstParamEnabled"); }
        }
        bool? fixFirstParamEnabled;

        public bool? Composite
        {
            get { return this.composite; }
            set { UpdateField(ref composite, value, "Composite"); }
        }
        bool? composite;

        public bool? NonDeliverableEnabled
        {
            get { return this.nonDeliverableEnabled; }
            set { UpdateField(ref nonDeliverableEnabled, value, "NonDeliverableEnabled"); }
        }
        bool? nonDeliverableEnabled;

        public bool? InitialiExchange
        {
            get { return this.initialiExchange; }
            set { UpdateField(ref initialiExchange, value, "InitialiExchange"); }
        }
        bool? initialiExchange;

        public bool? CalculatorIRCorrection
        {
            get { return this.calculatorIRCorrection; }
            set { UpdateField(ref calculatorIRCorrection, value, "CalculatorIRCorrection"); }
        }
        bool? calculatorIRCorrection;

        public bool? CalculatorFWD
        {
            get { return this.calculatorFWD; }
            set { UpdateField(ref calculatorFWD, value, "CalculatorFWD"); }
        }
        bool? calculatorFWD;

        public bool? MultipleExchange
        {
            get { return this.multipleExchange; }
            set { UpdateField(ref multipleExchange, value, "MultipleExchange"); }
        }
        bool? multipleExchange;

        public bool? CalculatorDelta
        {
            get { return this.calculatorDelta; }
            set { UpdateField(ref calculatorDelta, value, "CalculatorDelta"); }
        }
        bool? calculatorDelta;

        public bool? NominalMarketDisrupt
        {
            get { return this.nominalMarketDisrupt; }
            set { UpdateField(ref nominalMarketDisrupt, value, "NominalMarketDisrupt"); }
        }
        bool? nominalMarketDisrupt;

        public bool? ManageRisk
        {
            get { return this.manageRisk; }
            set { UpdateField(ref manageRisk, value, "ManageRisk"); }
        }
        bool? manageRisk;

        public int? FixFirstSetting
        {
            get { return this.fixFirstSetting; }
            set { UpdateField(ref fixFirstSetting, value, "FixFirstSetting"); }
        }
        int? fixFirstSetting;

        public int? ExpectedN
        {
            get { return this.expectedN; }
            set { UpdateField(ref expectedN, value, "ExpectedN"); }
        }
        int? expectedN;

        public int? PaymentLag
        {
            get { return this.paymentLag; }
            set { UpdateField(ref paymentLag, value, "PaymentLag"); }
        }
        int? paymentLag;

        public int? FixFinalSetting
        {
            get { return this.fixFinalSetting; }
            set { UpdateField(ref fixFinalSetting, value, "FixFinalSetting"); }
        }
        int? fixFinalSetting;

        public int? RollDay
        {
            get { return this.rollDay; }
            set { UpdateField(ref rollDay, value, "RollDay"); }
        }
        int? rollDay;

        public int? CalculatorStrike
        {
            get { return this.calculatorStrike; }
            set { UpdateField(ref calculatorStrike, value, "CalculatorStrike"); }
        }
        int? calculatorStrike;

        public int? Periods
        {
            get { return this.periods; }
            set { UpdateField(ref periods, value, "Periods"); }
        }
        int? periods;

        public int? Annualization
        {
            get { return this.annualization; }
            set { UpdateField(ref annualization, value, "Annualization"); }
        }
        public int? annualization;

        public int? CalculatorStrikeUsed
        {
            get { return this.calculatorStrikeUsed; }
            set { UpdateField(ref calculatorStrikeUsed, value, "CalculatorStrikeUsed"); }
        }
        public int? calculatorStrikeUsed;

        public int? CalculatorVolOfVol
        {
            get { return this.calculatorVolOfVol; }
            set { UpdateField(ref calculatorVolOfVol, value, "CalculatorVolOfVol"); }
        }
        public int? calculatorVolOfVol;

        public int? CalculatorVolSpread
        {
            get { return this.calculatorVolSpread; }
            set { UpdateField(ref calculatorVolSpread, value, "CalculatorVolSpread"); }
        }
        public int? calculatorVolSpread;

        public DateTimeOffset? SubstractDays
        {
            get { return this.substractDays; }
            set { UpdateField(ref substractDays, value, "SubstractDays"); }
        }
        public DateTimeOffset? substractDays;

        public DateTime? EffectiveDate
        {
            get { return this.effectiveDate; }
            set { UpdateField(ref effectiveDate, value, "EffectiveDate"); }
        }
        public DateTime? effectiveDate;

        public DateTime? RegularDatesFirst
        {
            get { return this.regularDatesFirst; }
            set { UpdateField(ref regularDatesFirst, value, "RegularDatesFirst"); }
        }
        public DateTime? regularDatesFirst;

        public DateTime? RegulatDatesLast
        {
            get { return this.regulatDatesLast; }
            set { UpdateField(ref regulatDatesLast, value, "RegulatDatesLast"); }
        }
        public DateTime? regulatDatesLast;

        public DateTime? ExpiryDate
        {
            get { return this.expiryDate; }
            set { UpdateField(ref expiryDate, value, "ExpiryDate"); }
        }
        public DateTime? expiryDate;

        public DateTimeOffset? ExpiryOffset
        {
            get { return this.expiryOffset; }
            set { UpdateField(ref expiryOffset, value, "ExpiryOffset"); }
        }
        public DateTimeOffset? expiryOffset;

        public DateTime? PaymentDate
        {
            get { return this.paymentDate; }
            set { UpdateField(ref paymentDate, value, "paymentDate"); }
        }
        public DateTime? paymentDate;

        public string NationalCurrency
        {
            get { return this.nationalCurrency; }
            set { UpdateField(ref nationalCurrency, value, "NationalCurrency"); }
        }
        string nationalCurrency;

        public string NonDeliverable
        {
            get { return this.nonDeliverable; }
            set { UpdateField(ref nonDeliverable, value, "NonDeliverable"); }
        }
        string nonDeliverable;

        public string ObsoleteCondition
        {
            get { return this.obsoleteCondition; }
            set { UpdateField(ref obsoleteCondition, value, "ObsoleteCondition"); }
        }
        string obsoleteCondition;

        public string Bit
        {
            get { return this.bit; }
            set { UpdateField(ref bit, value, "Bit"); }
        }
        string bit;

        public string Frequency
        {
            get { return this.frequency; }
            set { UpdateField(ref frequency, value, "Frequency"); }
        }
        string frequency;

        public string RollingMCD
        {
            get { return this.rollingMCD; }
            set { UpdateField(ref rollingMCD, value, "RollingMCD"); }
        }
        string rollingMCD;

        public string CalculatorName
        {
            get { return this.calculatorName; }
            set { UpdateField(ref calculatorName, value, "CalculatorName"); }
        }
        string calculatorName;

        public string ExpiryDataCallendar
        {
            get { return this.expiryDataCallendar; }
            set { UpdateField(ref expiryDataCallendar, value, "ExpiryDataCallendar"); }
        }
        string expiryDataCallendar;

        public string DetailsBook
        {
            get { return this.detailsBook; }
            set { UpdateField(ref detailsBook, value, "DetailsBook"); }
        }
        string detailsBook;

        public string DetailsTrader
        {
            get { return this.detailsTrader; }
            set { UpdateField(ref detailsTrader, value, "DetailsTrader"); }
        }
        string detailsTrader;

        public string DetailsMarketer
        {
            get { return this.detailsMarketer; }
            set { UpdateField(ref detailsMarketer, value, "DetailsMarketer"); }
        }
        string detailsMarketer;

        public string DetailsCptyCode
        {
            get { return this.detailsCptyCode; }
            set { UpdateField(ref detailsCptyCode, value, "DetailsCptyCode"); }
        }
        string detailsCptyCode;

        public string DetailsCounterBook
        {
            get { return this.detailsCounterBook; }
            set { UpdateField(ref detailsCounterBook, value, "DetailsCounterBook"); }
        }
        string detailsCounterBook;

        public string DetailsAgent
        {
            get { return this.detailsAgent; }
            set { UpdateField(ref detailsAgent, value, "DetailsAgent"); }
        }
        string detailsAgent;

        public string DetailsBrokerCode
        {
            get { return this.detailsBrokerCode; }
            set { UpdateField(ref detailsBrokerCode, value, "DetailsBrokerCode"); }
        }
        string detailsBrokerCode;

        public string EconomicComments
        {
            get { return this.economicComments; }
            set { UpdateField(ref economicComments, value, "EconomicComments"); }
        }
        string economicComments;

        public string Expiry
        {
            get { return this.expiry; }
            set { UpdateField(ref expiry, value, "Expiry"); }
        }
        string expiry;

        public string PaymentCallendar
        {
            get { return this.paymentCallendar; }
            set { UpdateField(ref paymentCallendar, value, "PaymentCallendar"); }
        }
        string paymentCallendar;

        public string PaymentCurrency
        {
            get { return this.paymentCurrency; }
            set { UpdateField(ref paymentCurrency, value, "PaymentCurrency"); }
        }
        string paymentCurrency;

        public string Contract
        {
            get { return this.contract; }
            set { UpdateField(ref contract, value, "Contract"); }
        }
        string contract;

        public string ProductType
        {
            get { return this.productType; }
            set { UpdateField(ref productType, value, "ProductType"); }
        }
        string productType;

        public string StructureType
        {
            get { return this.structureType; }
            set { UpdateField(ref structureType, value, "StructureType"); }
        }
        string structureType;

        public string IndexName
        {
            get { return this.indexName; }
            set { UpdateField(ref indexName, value, "IndexName"); }
        }
        string indexName;

        public string SwapType
        {
            get { return this.swapType; }
            set { UpdateField(ref swapType, value, "SwapType"); }
        }
        string swapType;



        private void UpdateField<T>(ref T oldVal, T newVal, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(oldVal, newVal) == true)
            {
                return;
            }

            oldVal = newVal;
            OnPropertyChanged(propertyName);
        }








        public static TradeHistory GetCopyOf(ITradeHistory iTHistory)
        {
            return new TradeHistory
            {
                Created = iTHistory.Created,
                TradeId = iTHistory.TradeId,
                Author = iTHistory.Author,
                Status = iTHistory.Status,
                Id = iTHistory.Id,

                VegaAmount = iTHistory.VegaAmount,
                Units = iTHistory.Units,
                Strike = iTHistory.Strike,
                VolumeCapture = iTHistory.VolumeCapture,
                Nominal = iTHistory.Nominal,
                ReferenceVolatility = iTHistory.ReferenceVolatility,
                CalculatorMinStrike = iTHistory.CalculatorMinStrike,
                CalculatorMaxString = iTHistory.CalculatorMaxString,
                Selling = iTHistory.Selling,
                AnnualizationEnabled = iTHistory.AnnualizationEnabled,
                FixFinalParamEnabled = iTHistory.FixFinalParamEnabled,
                ExpectedNEnabled = iTHistory.ExpectedNEnabled,
                VolumeCaptureEnabled = iTHistory.VolumeCaptureEnabled,
                FixFirstParamEnabled = iTHistory.FixFirstParamEnabled,
                Composite = iTHistory.Composite,
                NonDeliverableEnabled = iTHistory.NonDeliverableEnabled,
                InitialiExchange = iTHistory.InitialiExchange,
                CalculatorIRCorrection = iTHistory.CalculatorIRCorrection,
                CalculatorFWD = iTHistory.CalculatorFWD,
                MultipleExchange = iTHistory.MultipleExchange,
                CalculatorDelta = iTHistory.CalculatorDelta,
                NominalMarketDisrupt = iTHistory.NominalMarketDisrupt,
                ManageRisk = iTHistory.ManageRisk,
                FixFirstSetting = iTHistory.FixFirstSetting,
                ExpectedN = iTHistory.ExpectedN,
                PaymentLag = iTHistory.PaymentLag,
                FixFinalSetting = iTHistory.FixFinalSetting,
                RollDay = iTHistory.RollDay,
                CalculatorStrike = iTHistory.CalculatorStrike,
                Periods = iTHistory.Periods,
                Annualization = iTHistory.Annualization,
                CalculatorStrikeUsed = iTHistory.CalculatorStrikeUsed,
                CalculatorVolOfVol = iTHistory.CalculatorVolOfVol,
                CalculatorVolSpread = iTHistory.CalculatorVolSpread,
                SubstractDays = iTHistory.SubstractDays,
                EffectiveDate = iTHistory.EffectiveDate,
                RegularDatesFirst = iTHistory.RegularDatesFirst,
                RegulatDatesLast = iTHistory.RegulatDatesLast,
                ExpiryDate = iTHistory.ExpiryDate,
                ExpiryOffset = iTHistory.ExpiryOffset,
                PaymentDate = iTHistory.PaymentDate,
                NationalCurrency = iTHistory.NationalCurrency,
                NonDeliverable = iTHistory.NonDeliverable,
                ObsoleteCondition = iTHistory.ObsoleteCondition,
                Bit = iTHistory.Bit,
                Frequency = iTHistory.Frequency,
                RollingMCD = iTHistory.RollingMCD,
                CalculatorName = iTHistory.CalculatorName,
                ExpiryDataCallendar = iTHistory.ExpiryDataCallendar,
                DetailsBook = iTHistory.DetailsBook,
                DetailsTrader = iTHistory.DetailsTrader,
                DetailsMarketer = iTHistory.DetailsMarketer,
                DetailsCptyCode = iTHistory.DetailsCptyCode,
                DetailsCounterBook = iTHistory.DetailsCounterBook,
                DetailsAgent = iTHistory.DetailsAgent,
                DetailsBrokerCode = iTHistory.DetailsBrokerCode,
                EconomicComments = iTHistory.EconomicComments,
                Expiry = iTHistory.Expiry,
                PaymentCallendar = iTHistory.PaymentCallendar,
                PaymentCurrency = iTHistory.PaymentCurrency,
                Contract = iTHistory.Contract,
                ProductType = iTHistory.ProductType,
                StructureType = iTHistory.StructureType,
                IndexName = iTHistory.IndexName,
                SwapType = iTHistory.SwapType
            };
        }

        public bool SaveNeeded(ITradeHistory rhs)
        {
            if (this.Status == rhs.Status &&
                this.VegaAmount == rhs.VegaAmount &&
                this.Strike == rhs.Strike &&
                this.VolumeCapture == rhs.VolumeCapture &&
                this.Nominal == rhs.Nominal &&
                this.ReferenceVolatility == rhs.ReferenceVolatility &&
                this.CalculatorMinStrike == rhs.CalculatorMinStrike &&
                this.CalculatorMaxString == rhs.CalculatorMaxString &&
                this.Units == rhs.Units &&
                this.Selling == rhs.Selling &&
                this.AnnualizationEnabled == rhs.AnnualizationEnabled &&
                this.FixFinalParamEnabled == rhs.FixFinalParamEnabled &&
                this.ExpectedNEnabled == rhs.ExpectedNEnabled &&
                this.VolumeCaptureEnabled == rhs.VolumeCaptureEnabled &&
                this.FixFirstParamEnabled == rhs.FixFirstParamEnabled &&
                this.Composite == rhs.Composite &&
                this.NonDeliverableEnabled == rhs.NonDeliverableEnabled &&
                this.InitialiExchange == rhs.InitialiExchange &&
                this.CalculatorIRCorrection == rhs.CalculatorIRCorrection &&
                this.CalculatorFWD == rhs.CalculatorFWD &&
                this.MultipleExchange == rhs.MultipleExchange &&
                this.CalculatorDelta == rhs.CalculatorDelta &&
                this.NominalMarketDisrupt == rhs.NominalMarketDisrupt &&
                this.ManageRisk == rhs.ManageRisk &&

                this.FixFirstSetting == rhs.FixFirstSetting &&
                this.ExpectedN == rhs.ExpectedN &&
                this.PaymentLag == rhs.PaymentLag &&
                this.FixFinalSetting == rhs.FixFinalSetting &&
                this.RollDay == rhs.RollDay &&
                this.CalculatorStrike == rhs.CalculatorStrike &&
                this.Periods == rhs.Periods &&
                this.Annualization == rhs.Annualization &&
                this.CalculatorStrikeUsed == rhs.CalculatorStrikeUsed &&
                this.CalculatorVolOfVol == rhs.CalculatorVolOfVol &&
                this.CalculatorVolSpread == rhs.CalculatorVolSpread &&

                this.SubstractDays == rhs.SubstractDays &&
                this.ExpiryOffset == rhs.ExpiryOffset &&

                this.EffectiveDate == rhs.EffectiveDate &&
                this.RegularDatesFirst == rhs.RegularDatesFirst &&
                this.RegulatDatesLast == rhs.RegulatDatesLast &&
                this.ExpiryDate == rhs.ExpiryDate &&
                this.PaymentDate == rhs.PaymentDate &&

                this.NationalCurrency == rhs.NationalCurrency &&
                this.NonDeliverable == rhs.NonDeliverable &&
                this.ObsoleteCondition == rhs.ObsoleteCondition &&
                this.bit == rhs.Bit &&
                this.Frequency == rhs.Frequency &&
                this.RollingMCD == rhs.RollingMCD &&
                this.CalculatorName == rhs.CalculatorName &&
                this.ExpiryDataCallendar == rhs.ExpiryDataCallendar &&
                this.DetailsBook == rhs.DetailsBook &&
                this.DetailsTrader == rhs.DetailsTrader &&
                this.DetailsMarketer == rhs.DetailsMarketer &&
                this.DetailsCptyCode == rhs.DetailsCptyCode &&
                this.DetailsCounterBook == rhs.DetailsCounterBook &&
                this.DetailsAgent == rhs.DetailsAgent &&
                this.DetailsBrokerCode == rhs.DetailsBrokerCode &&
                this.EconomicComments == rhs.EconomicComments &&
                this.Expiry == rhs.Expiry &&
                this.PaymentCallendar == rhs.PaymentCallendar &&
                this.PaymentCurrency == rhs.PaymentCurrency &&
                this.Contract == rhs.Contract &&
                this.ProductType == rhs.ProductType &&
                this.StructureType == rhs.StructureType &&
                this.IndexName == rhs.IndexName &&
                this.SwapType == rhs.SwapType)
            {
                return false;
            }
            return true;


        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var h = this.PropertyChanged;
            if (h != null)
            {
                h(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
