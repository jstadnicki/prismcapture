﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Controls;

namespace Capture.Common.Model
{
    public enum AccountType
    {
        Junior = 1,
        Regular,
        Senior,
        Manager,
        Administrator
    }

    public interface ITradeUser
    {
        int Id { get; }
        string Username { get; }
        string HashPassword { get; }
        string FirstName { get; }
        string LastName { get; }
        string FullName { get; }
        AccountType Account { get; }
        byte[] UserPicture { get; }
    }
}
