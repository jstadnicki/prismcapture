﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capture.Common.Model
{
    public interface ITradeListElemet
    {
        int TradeId { get; }
        DateTime Crated { get; }
        string Author { get; }
        IEnumerable<ITradeHistory> Histories { get; }
    }
}
