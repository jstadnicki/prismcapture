﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public interface ITradeCapture: INotifyPropertyChanged
    {
        int AuthorId { get; }
        int TradeId { get; }
        DateTime Created { get; }
        int ViewId { get; }
        IEnumerable<ITradeHistory> Histories { get; }
        TradeHistory Current { get; }
    }
}
