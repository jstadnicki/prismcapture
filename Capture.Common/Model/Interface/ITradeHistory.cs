﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public interface ITradeHistory
    {
        DateTime Created { get; }
        int TradeId { get; }
        int Id { get; }
        TradeStatus Status { get; }

        Nullable<decimal> VegaAmount { get; }
        Nullable<decimal> Strike { get; }
        Nullable<decimal> VolumeCapture { get; }
        Nullable<decimal> Nominal { get; }
        Nullable<decimal> ReferenceVolatility { get; }
        Nullable<decimal> CalculatorMinStrike { get; }
        Nullable<decimal> CalculatorMaxString { get; }

        Nullable<bool> Selling { get; }
        Nullable<bool> AnnualizationEnabled { get; }
        Nullable<bool> FixFinalParamEnabled { get; }
        Nullable<bool> ExpectedNEnabled { get; }
        Nullable<bool> VolumeCaptureEnabled { get; }
        Nullable<bool> FixFirstParamEnabled { get; }
        Nullable<bool> Composite { get; }
        Nullable<bool> NonDeliverableEnabled { get; }
        Nullable<bool> InitialiExchange { get; }
        Nullable<bool> CalculatorIRCorrection { get; }
        Nullable<bool> CalculatorFWD { get; }
        Nullable<bool> MultipleExchange { get; }
        Nullable<bool> CalculatorDelta { get; }
        Nullable<bool> NominalMarketDisrupt { get; }
        Nullable<bool> ManageRisk { get; }

        Nullable<int> FixFirstSetting { get; }
        Nullable<int> ExpectedN { get; }
        Nullable<int> PaymentLag { get; }
        Nullable<int> FixFinalSetting { get; }
        Nullable<int> RollDay { get; }
        Nullable<int> CalculatorStrike { get; }
        Nullable<int> Periods { get; }
        Nullable<int> Annualization { get; }
        Nullable<int> CalculatorStrikeUsed { get; }
        Nullable<int> CalculatorVolOfVol { get; }
        Nullable<int> CalculatorVolSpread { get; }
        Nullable<int> Units { get; }


        Nullable<DateTimeOffset> SubstractDays { get; }
        Nullable<DateTimeOffset> ExpiryOffset { get; }

        Nullable<DateTime> EffectiveDate { get; }
        Nullable<DateTime> RegularDatesFirst { get; }
        Nullable<DateTime> RegulatDatesLast { get; }
        Nullable<DateTime> ExpiryDate { get; }
        Nullable<DateTime> PaymentDate { get; }

        string NationalCurrency { get; }
        string NonDeliverable { get; }
        string ObsoleteCondition { get; }
        string Bit { get; }
        string Frequency { get; }
        string RollingMCD { get; }
        string CalculatorName { get; }
        string ExpiryDataCallendar { get; }
        string DetailsBook { get; }
        string DetailsTrader { get; }
        string DetailsMarketer { get; }
        string DetailsCptyCode { get; }
        string DetailsCounterBook { get; }
        string DetailsAgent { get; }
        string DetailsBrokerCode { get; }
        string EconomicComments { get; }
        string Expiry { get; }
        string PaymentCallendar { get; }
        string PaymentCurrency { get; }
        string Contract { get; }
        string ProductType { get; }
        string StructureType { get; }
        string IndexName { get; }
        string SwapType { get; }        
        string Author { get; }

        bool SaveNeeded(ITradeHistory tradeHistory);

    }
}
