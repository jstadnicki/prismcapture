﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Capture.Common.Model
{
    public sealed class TradeCapture : ITradeCapture
    {
        public IEnumerable<ITradeHistory> Histories { get; set; }

        TradeHistory current;
        public TradeHistory Current
        {
            get { return this.current; }
            set
            {
                if (current == value)
                {
                    return;
                }
                if (current != null)
                {
                    current.PropertyChanged -= this.HistoryChanged;
                }
                
                current = value;
                current.PropertyChanged += this.HistoryChanged;
            }
        }

        private void HistoryChanged(object sender, PropertyChangedEventArgs args)
        {
            var e = PropertyChanged;
            if (e != null)
            {
                e(this, new PropertyChangedEventArgs("Current"));
            }
        }

        public int AuthorId { get; set; }

        public int TradeId { get; set; }

        public DateTime Created { get; set; }

        public int ViewId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
