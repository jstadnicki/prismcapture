﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common;
using Capture.Common.Model;

namespace Capture.Common.Repository
{
    public interface IRepository
    {
        IEnumerable<ITradeListElemet> GetTradeList(int startIndex = 0, int count = int.MaxValue, TradeFilter filter = null);

        ITradeCapture GetTrade(int tradeId);
        void Save(ITradeCapture trade);
        int GetTradeCount(TradeFilter filter = null);
        ITradeUser LoadUserByName(string username);
    } 
}
