﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capture.Common.Configuration
{
    public interface ICaptureStatus
    {
        int NewId { get; }
        int ProposedId { get; }
        int AcceptedId { get; }
        int RejectedId { get; }
        int UnderReviewId { get; }
        int ExecutedId { get; }
        int DeletedId { get; }
    }
}
