﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using Capture.Common.Model;

namespace Capture.Common.Messages
{
    public enum ApplicationCommandsType
    {
        SaveTrade = 1,
        CloseApplication,
        InitializeApplication,
        LoadTradeWithId
    }

    public class TradeApplicationCommand
    {
        public ApplicationCommandsType CommandType { get; set; }
        public int LoadTradeId { get; set; }
    }

    public class TradeCaptureApplicationCommands : CompositePresentationEvent<TradeApplicationCommand>
    {
        
    }

    public class LoginCaptureUser : CompositePresentationEvent<ITradeUser>
    { }
}
