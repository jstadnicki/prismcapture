﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism;

namespace Capture.Common.Prism
{
    public sealed class PrismNavigation : INavigation
    {
        private IRegionManager regionManager;

        public PrismNavigation(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
        }

        public IRegionManager AddToRegion(string regionName, object view)
        {
            return this.regionManager.AddToRegion(regionName, view);
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Func<object> getContentDelegate)
        {
            return this.regionManager.RegisterViewWithRegion(regionName, getContentDelegate);
        }

        public IRegionManager RegisterViewWithRegion(string regionName, Type viewType)
        {
            return this.regionManager.RegisterViewWithRegion(regionName, viewType);
        }

        public void RequestNavigate(string regionName, string source)
        {
            this.regionManager.RequestNavigate(regionName, source);
        }

        public void RequestNavigate(string regionName, Uri source)
        {
            this.regionManager.RequestNavigate(regionName, source);
        }

        public void RequestNavigate(string regionName, Uri source, Action<NavigationResult> navigationCallback)
        {
            this.regionManager.RequestNavigate(regionName, source, navigationCallback);
        }

        public void RequestNavigate(string regionName, string source, Action<NavigationResult> navigationCallback)
        {
            this.regionManager.RequestNavigate(regionName, source, navigationCallback);
        }

        // my own
        public void RequestNavigateWithTradeIdAsParam(string baseAddress, int id = -1)
        {
            var uriQuery = new UriQuery();
            uriQuery.Add(PareterNavigationNames.TradeId, id.ToString());
            var uriAddress = new Uri(baseAddress + uriQuery, UriKind.Relative);
            this.regionManager.NavigationService().RequestNavigate(RegionNames.CenterFillRegion, uriAddress);
        }
    }
}
