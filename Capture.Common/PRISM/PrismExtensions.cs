﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Capture.Common.Prism
{
    public static class PrismExtensions
    {
        static PrismExtensions()
        {
            ServiceFactory = p => new PrismNavigation(p);
        }
        
        public static Func<Microsoft.Practices.Prism.Regions.IRegionManager, INavigation> ServiceFactory { get; set; }

        public static INavigation NavigationService(this Microsoft.Practices.Prism.Regions.IRegionManager regionManager)
        {
            return ServiceFactory(regionManager);
        }
    }
}
