﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Regions;

namespace Capture.Common.Prism
{
    public interface INavigation
    {
        IRegionManager AddToRegion(string regionName, object view);
        IRegionManager RegisterViewWithRegion(string regionName, Func<object> getContentDelegate);
        IRegionManager RegisterViewWithRegion(string regionName, Type viewType);
        void RequestNavigate(string regionName, string source);
        void RequestNavigate(string regionName, Uri source);
        void RequestNavigate(string regionName, string source, Action<NavigationResult> navigationCallback);
        void RequestNavigate(string regionName, Uri source, Action<NavigationResult> navigationCallback);

        // my own
        void RequestNavigateWithTradeIdAsParam(string baseAddress, int id = -1);

    }
}
