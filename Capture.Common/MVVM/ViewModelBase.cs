﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows;

namespace Capture.Common.MVVM
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected Dispatcher CurrentDispatcher
        {
            get
            {
                if (Application.Current != null)
                {
                    return Application.Current.Dispatcher;
                }
                if (Dispatcher.CurrentDispatcher != null)
                {
                    return Dispatcher.CurrentDispatcher;
                }

                throw new InvalidProgramException("VM cannot work without a dispatcher");
            }
        }

        protected void OnPropertyChanged(string propety)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propety));
            }
        }

    }
}
