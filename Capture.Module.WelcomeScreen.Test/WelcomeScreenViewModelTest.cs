﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using Capture.Module.WelcomeScreen.MVVM;
using Capture.Common.Repository;
using Capture.Common.Model;
using System.Threading;
using System.Threading.Tasks;


namespace Capture.Module.WelcomeScreen.Test
{
    [TestFixture]
    public class WelcomeScreenViewModelTest
    {
        public static string WELCOME_SCREEN_USER_FIRST_NAME = "";
        public static string WELCOME_SCREEN_USER_LAST_NAME = "";
        public static int WELCOME_SCREEN_USER_ID = -1;
        public static int GET_TRADE_LIST_EXECUTED_COUNT = -1;

        public static int NEW_DOCUMENTS_COUNT = -1;
        public static int ACCEPTED_DOCUMENTS_COUNT = -1;
        public static int EXECUTED_DOCUMENTS_COUNT = -1;
        public static int PROPOSED_DOCUMENTS_COUNT = -1;
        public static int REJECTED_DOCUMENTS_COUNT = -1;
        public static int UNDER_REVIEW_DOCUMENTS_COUNT = -1;

        public static ManualResetEvent NEW_DOCUMENTS_EVENT = new ManualResetEvent(false);
        public static ManualResetEvent ACCEPTED_DOCUMENTS_EVENT = new ManualResetEvent(false);
        public static ManualResetEvent EXECUTED_DOCUMENTS_EVENT = new ManualResetEvent(false);
        public static ManualResetEvent PROPOSED_DOCUMENTS_EVENT = new ManualResetEvent(false);
        public static ManualResetEvent REJECTED_DOCUMENTS_EVENT = new ManualResetEvent(false);
        public static ManualResetEvent UNDER_REVIEW_DOCUMENTS_EVENT = new ManualResetEvent(false);

        public static ManualResetEvent LAST_MODIFICATION_AFTER_EVENT = new ManualResetEvent(false);

        public static int LAST_MODIFIED_COUNT = -1;

        private UnityContainer container;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.container = new UnityContainer();
            this.container.RegisterType<IWelcomeScreenViewModel, WelcomeScreenViewModel>();
            this.container.RegisterType<IRepository, WelcomeScreenRepositoryMock>();
            this.container.RegisterType<ITradeUser, WelcomeSceenTradeUser>();
        }

        [Test]
        public void T000_Should_Implement_IWelcomScreenViewModel_Interface()
        {
            var result = this.container.Resolve<IWelcomeScreenViewModel>();
            Assert.NotNull(result);
        }

        [Test]
        public void T001_When_Successfuly_Constructed_Shuld_Gather_All_Necessary_Data_About_Currently_User_From_Repository()
        {
            // arrange

            WelcomeScreenViewModelTest.NEW_DOCUMENTS_COUNT = 12;
            WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_COUNT = 13;
            WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_COUNT = 14;
            WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_COUNT = 15;
            WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_COUNT = 16;
            WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_COUNT = 17;


            WelcomeScreenViewModelTest.NEW_DOCUMENTS_EVENT.Reset();
            WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_EVENT.Reset();
            WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_EVENT.Reset();
            WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_EVENT.Reset();
            WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_EVENT.Reset();
            WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_EVENT.Reset();


            WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_FIRST_NAME = "janusz";
            WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_LAST_NAME = "krzysztof";
            WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_ID = 667;
            WelcomeScreenViewModelTest.GET_TRADE_LIST_EXECUTED_COUNT = 0;

            var result = this.container.Resolve<IWelcomeScreenViewModel>();
            var user = this.container.Resolve<ITradeUser>();

            // assert
            Assert.That(result.CurrentDate, Is.EqualTo(DateTime.Now.ToLongDateString()));
            Assert.That(result.CurrentFullDayName, Is.EqualTo(DateTime.Now.DayOfWeek.ToString()));
            Assert.That(result.CurrentTime, Is.EqualTo(DateTime.Now.ToLongTimeString()));
            Assert.That(result.UserFullName, Is.EqualTo(string.Format("{0} {1}",
                WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_FIRST_NAME,
                WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_LAST_NAME)));
            Assert.That(result.UserId, Is.EqualTo(WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_ID));
            Assert.That(result.UserImage, Is.EqualTo(user.UserPicture));


            ManualResetEvent.WaitAll(new WaitHandle[]
            {
                WelcomeScreenViewModelTest.NEW_DOCUMENTS_EVENT,
                WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_EVENT,
                WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_EVENT,
                WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_EVENT,
                WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_EVENT,
                WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_EVENT
            }, 5000);
            Thread.Yield();
            Thread.Sleep(5000);


            Assert.AreEqual(WelcomeScreenViewModelTest.NEW_DOCUMENTS_COUNT, result.NewDocumentsCount);
            Assert.AreEqual(WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_COUNT, result.AcceptedDocumtnsCount);
            Assert.AreEqual(WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_COUNT, result.ExecutedDocumentsCount);
            Assert.AreEqual(WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_COUNT, result.ProposedDocumentsCount);
            Assert.AreEqual(WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_COUNT, result.RejectedDocumentsCount);
            Assert.AreEqual(WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_COUNT, result.UnderReviewDocumentsCount);
        }

        [Test]
        public void T002_When_Successfuly_Constructed_Display_List_Of_Trades_That_Were_Modified_During_Last_7_Days()
        {
            // arrange
            WelcomeScreenViewModelTest.LAST_MODIFIED_COUNT = 15;
            WelcomeScreenViewModelTest.LAST_MODIFICATION_AFTER_EVENT.Reset();

            var result = this.container.Resolve<IWelcomeScreenViewModel>();
            var user = this.container.Resolve<ITradeUser>();
            IEnumerable<string> lastAccessedTrades = new List<string>();

            WelcomeScreenViewModelTest.LAST_MODIFICATION_AFTER_EVENT.WaitOne(1500);
            Thread.Sleep(150);

            Assert.AreEqual(WelcomeScreenViewModelTest.LAST_MODIFIED_COUNT, result.LastModifiedDocuments.Count());
        }

        [Test]
        public void T003_When_Tab_With_Recent_Modification_Is_Changed_Should_Display_Busy_Window_And_Load_More_Less_Recent_Modifications_From_Repository()
        {
            // arrange
            var sut = this.container.Resolve<IWelcomeScreenViewModel>();

            // act
            while (sut.RecentModificationIsBusy == true)
            {
                Thread.Sleep(50);
            }
            var recentWeek = sut.LastModifiedDocuments.Count();

            sut.SelectedTabControlIndex = 1;
            while (sut.RecentModificationIsBusy == true)
            {
                Thread.Sleep(50);
            }
            var recentMonth = sut.LastModifiedDocuments.Count();

            sut.SelectedTabControlIndex = 2;
            while (sut.RecentModificationIsBusy == true)
            {
                Thread.Sleep(50);
            }
            var recentQuarter = sut.LastModifiedDocuments.Count();

            Assert.GreaterOrEqual(recentQuarter, recentMonth);
            Assert.GreaterOrEqual(recentMonth, recentWeek);
        }

        [Test]
        public void T004_When_No_Recent_Document_Is_Selected_LoadSelectedRecentDocument_Command_Button_Should_Be_Disabled_And_Shouldnt_Be_Executable()
        {
            // arrange
            var sut = this.container.Resolve<IWelcomeScreenViewModel>();

            // act
            bool result = sut.LoadSelectedRecentDocumentCommand.CanExecute();

            // assert
            Assert.False(result);            
        }

        [Test]
        public void T005_When_Recent_Document_Is_Selected_LoadSelectedRecentDocument_Command_Button_Should_Be_Enabled_And_Should_Be_Executable()
        {
            // arrange
            var sut = this.container.Resolve<IWelcomeScreenViewModel>();
            sut.LastModifiedDocumentSelected = new TradeListElement();
            
            // act
            bool result = sut.LoadSelectedRecentDocumentCommand.CanExecute();

            // assert
            Assert.True(result);
        }

    }

    public class WelcomeScreenRepositoryMock : IRepository
    {
        public IEnumerable<ITradeListElemet> GetTradeList(int startIndex = 0, int count = int.MaxValue, TradeFilter filter = null)
        {
            if (filter.LastModificationAfter.HasValue)
            {
                var r = CreateDummyTradeListElements(WelcomeScreenViewModelTest.LAST_MODIFIED_COUNT);
                WelcomeScreenViewModelTest.LAST_MODIFICATION_AFTER_EVENT.Set();
                return r;
            }

            return null;
        }

        private IEnumerable<ITradeListElemet> CreateDummyTradeListElements(int p)
        {
            List<ITradeListElemet> l = new List<ITradeListElemet>();
            for (int i = 0; i < p; i++)
            {
                l.Add(new TradeListElement());
            }
            return l;
        }

        public Common.Model.ITradeCapture GetTrade(int tradeId)
        {
            throw new NotImplementedException();
        }

        public void Save(Common.Model.ITradeCapture trade)
        {
            throw new NotImplementedException();
        }

        public int GetTradeCount(TradeFilter filter = null)
        {
            WelcomeScreenViewModelTest.GET_TRADE_LIST_EXECUTED_COUNT++;
            if (filter != null)
            {
                if (filter.Status.HasValue == true)
                {
                    switch (filter.Status)
                    {
                        case TradeStatus.NewTrade:
                            WelcomeScreenViewModelTest.NEW_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.NEW_DOCUMENTS_COUNT;
                        case TradeStatus.UnderReview:
                            WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.UNDER_REVIEW_DOCUMENTS_COUNT;
                        case TradeStatus.Proposed:
                            WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.PROPOSED_DOCUMENTS_COUNT;
                        case TradeStatus.Accepted:
                            WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.ACCEPTED_DOCUMENTS_COUNT;
                        case TradeStatus.Rejected:
                            WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.REJECTED_DOCUMENTS_COUNT;
                        case TradeStatus.Deleted:
                            throw new ArgumentException("Filter.Status");
                        case TradeStatus.Executed:
                            WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_EVENT.Set();
                            return WelcomeScreenViewModelTest.EXECUTED_DOCUMENTS_COUNT;
                        default:
                            throw new ArgumentException("Filter.Status");
                    }
                }
            }

            return -1;
        }

        public Common.Model.ITradeUser LoadUserByName(string username)
        {
            throw new NotImplementedException();
        }
    }

    public class WelcomeSceenTradeUser : ITradeUser
    {
        public int Id
        {
            get { return WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_ID; }
        }

        public string Username
        {
            get { throw new NotImplementedException(); }
        }

        public string HashPassword
        {
            get { throw new NotImplementedException(); }
        }

        public string FullName
        {
            get { return string.Format("{0} {1}", this.FirstName, this.LastName); }
        }

        public AccountType Account
        {
            get { throw new NotImplementedException(); }
        }


        public string FirstName
        {
            get { return WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_FIRST_NAME; }
        }

        public string LastName
        {
            get { return WelcomeScreenViewModelTest.WELCOME_SCREEN_USER_LAST_NAME; }
        }

        public byte[] UserPicture
        {
            get { return null; }
        }
    }
}
