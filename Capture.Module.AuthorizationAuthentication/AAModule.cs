﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using Capture.Module.AuthorizationAuthentication.MVVM;
using Capture.Common.Prism;


namespace Capture.Module.AuthorizationAuthentication
{
    public class AAModule : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;

        public AAModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<ILoginViewModel, LoginViewModel>();
            this.container.RegisterType<object, LoginView>(typeof(ILoginView).FullName);

            this.RegisterViews();
        }

        private void RegisterViews()
        {
            this.regionManager.NavigationService().RegisterViewWithRegion(Capture.Common.RegionNames.CenterFillRegion,
                typeof(LoginView));
        }
    }
}
