﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using Capture.Common.Repository;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using Capture.Common.MVVM;
using Microsoft.Practices.Prism.Events;
using System.Threading;
using Capture.Common.Messages;

namespace Capture.Module.AuthorizationAuthentication.MVVM
{
    public class LoginViewModel : ViewModelBase, ILoginViewModel, INotifyPropertyChanged
    {
        string password;
        private IRepository repository;
        string username;
        string errorMessage;
        IEventAggregator eventAggregator;
        bool isBusy;
        private string loginErrorMessage;
        private Xceed.Wpf.Toolkit.WindowState loginErrorMessageVisibility;

        public LoginViewModel(IRepository repository, IEventAggregator eventAggregator)
        {
            this.CreateCommands();
            this.repository = repository;
            this.eventAggregator = eventAggregator;
            this.Password = null;
            this.Password = null;
        }

        private void CreateCommands()
        {
            this.LoginCommand = new DelegateCommand(this.LoginCommandExecute, this.LoginCommandCanExecute);
            this.CloseApplication = new DelegateCommand(this.CloseApplicationCommandExecute);
            this.LoginErrorOKCommand = new DelegateCommand(this.LoginErrorOKCommandExecute);
        }

        public DelegateCommand LoginCommand { get; set; }

        public DelegateCommand CloseApplication { get; set; }
        public DelegateCommand LoginErrorOKCommand { get; set; }


        public string Username
        {
            get { return this.username; }
            set
            {
                this.username = value;
                ValidateInputs();
                OnPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return this.password; }
            set
            {
                this.password = value;
                ValidateInputs();
                OnPropertyChanged("Password");
            }
        }

        public bool IsBusy
        {
            get { return this.isBusy; }
            set
            {
                this.isBusy = value;
                OnPropertyChanged("IsBusy");
            }            
        }


        private void ValidateInputs()
        {
            StringBuilder error = new StringBuilder();

            if (string.IsNullOrWhiteSpace(this.Username))
            {
                error.Append("Username cannot be empty\n");
            }

            if (string.IsNullOrWhiteSpace(this.Password))
            {
                error.Append("Password cannot be empty");
            }

            this.ErrorMessage = error.ToString();
        }

        public string ErrorMessage
        {
            get { return this.errorMessage; }
            set
            {
                this.errorMessage = value;
                OnPropertyChanged("ErrorMessage");
                this.LoginCommand.RaiseCanExecuteChanged();
            }
        }


        private void LoginCommandExecute()
        {
            IsBusy = true;
            string hashpassword = this.HashPassword(this.Password);
            this.ValidateUserAsync(hashpassword);            
        }

        private void ValidateUserAsync(string hashedPassword)
        {
            ThreadPool.QueueUserWorkItem(o =>
                {
                    var user = this.repository.LoadUserByName(this.Username);
                    bool valid = false;
                    if (user != null)
                    {
                        if (user.HashPassword == hashedPassword)
                        {
                            valid = true;
                        }
                    }
                    if (valid == true)
                    {
                        this.eventAggregator.GetEvent<LoginCaptureUser>().Publish(user);
                    }
                    else
                    {
                        IsBusy = false;
                        this.LoginErrorMessage = "User not found, username or password invalid";
                        this.LoginErrorMessageVisibility = Xceed.Wpf.Toolkit.WindowState.Open;
                        
                    }
                });            
        }

        public string LoginErrorMessage
        {
            get { return this.loginErrorMessage; }
            set
            {
                this.loginErrorMessage = value;
                OnPropertyChanged("");
            }
        }

        private string HashPassword(string p)
        {
            //TODO
            return p;
        }

        private bool LoginCommandCanExecute()
        {
            return string.IsNullOrWhiteSpace(this.ErrorMessage);
        }

        private void CloseApplicationCommandExecute()
        {
            this.eventAggregator
                .GetEvent<TradeCaptureApplicationCommands>()
                .Publish(new TradeApplicationCommand 
                    { CommandType = ApplicationCommandsType.CloseApplication });
        }

        private void LoginErrorOKCommandExecute()
        {
            this.LoginErrorMessageVisibility = Xceed.Wpf.Toolkit.WindowState.Closed;
        }

        public void PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.Password = (sender as PasswordBox).Password;
        }

        string ILoginViewModel.ErrorMessage
        {
            get { return this.errorMessage; }
            set { this.errorMessage = value; }
        }


        public Xceed.Wpf.Toolkit.WindowState LoginErrorMessageVisibility
        {
            get { return this.loginErrorMessageVisibility; }
            set
            {
                this.loginErrorMessageVisibility = value;
                OnPropertyChanged("LoginErrorMessageVisibility");
            }


        }
    }
}
