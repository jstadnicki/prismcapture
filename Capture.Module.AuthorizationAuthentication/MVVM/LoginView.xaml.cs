﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Capture.Module.AuthorizationAuthentication.MVVM
{
    public partial class LoginView : UserControl, ILoginView
    {
        public LoginView(ILoginViewModel vm)
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void OnPasswordBoxPasswordChanged(object sender, RoutedEventArgs e)
        {
            (this.DataContext as ILoginViewModel).PasswordChanged(sender, e);
        }
    }
}
