﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Commands;
using System.Windows;

namespace Capture.Module.AuthorizationAuthentication.MVVM
{
    public interface ILoginViewModel
    {
        void PasswordChanged(object sender, RoutedEventArgs e);
        bool IsBusy { get; }
        Xceed.Wpf.Toolkit.WindowState LoginErrorMessageVisibility { get; }

        DelegateCommand LoginCommand { get; }
        DelegateCommand CloseApplication { get; }
        DelegateCommand LoginErrorOKCommand { get; }

        string Username { get; set; }
        string Password { get; set; }
        string ErrorMessage { get; set; }
        string LoginErrorMessage { get; }
    }
}
