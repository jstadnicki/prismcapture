﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Capture.Module.AuthorizationAuthentication.Test
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            NUnit.Gui.AppEntry.Main(new string[] {
                Assembly.GetExecutingAssembly().Location });
        }
    }
}
