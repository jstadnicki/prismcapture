﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Practices.Unity;
using Capture.Module.AuthorizationAuthentication.MVVM;
using Capture.Common.Repository;
using Microsoft.Practices.Prism.Events;
using System.Threading;
using Capture.Common.Model;

namespace Capture.Module.AuthorizationAuthentication.Test
{
    [TestFixture]
    public class LoginViewModelTest
    {

        public static int REPOSITORY_ACCESS_TIME_IN_MS = 500;
        public static string REPOSITORY_USER_HASHED_PASSWORD;
        public static bool REPOSITORY_SHOULD_FIND_USER = false;
        public static bool EVENTAGGREGATOR_USED;

        private UnityContainer container;
        [TestFixtureSetUp]
        public void FixtureSetupe()
        {
            this.container = new UnityContainer();
            this.container.RegisterType<ILoginViewModel, LoginViewModel>();
            this.container.RegisterType<IRepository, LoginViewModelMockRepository>();
            this.container.RegisterType<IEventAggregator, LoginViewModelMockEventAggregator>();
        }

        [Test]
        public void T000_ViewModel_Should_Implement_ViewModel_Interface()
        {
            // arrange
            // act
            var result = this.container.Resolve<ILoginViewModel>();
            // assert
            Assert.NotNull(result);
        }

        [Test]
        public void T001_When_Username_And_Password_Not_Set_Should_Display_Inforamation_About_Inputs_Missing()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // assert
            Assert.That(sut.ErrorMessage, Is.EqualTo("Username cannot be empty\nPassword cannot be empty"));
        }

        [Test]
        public void T002_When_Username_And_Password_Not_Set_Login_Command_Should_Not_Be_Able_To_Execute()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // assert
            Assert.False(sut.LoginCommand.CanExecute());
        }

        [Test]
        public void T003_When_Username_And_Password_Not_Set_Close_Command_Should_Be_Executable()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // assert
            Assert.True(sut.CloseApplication.CanExecute());
        }

        [Test]
        public void T004_When_Username_Is_Set_And_Missing_The_Password_Only_Password_Related_Info_Should_Be_Displaed_To_The_User()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // act
            sut.Username = "ignore";
            // assert
            Assert.That(sut.ErrorMessage, Is.EqualTo("Password cannot be empty"));
        }

        [Test]
        public void T005_When_Password_Is_Set_And_Missing_The_Username_Only_Username_Related_Info_Should_Be_Displaed_To_The_User()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // act
            sut.Password = "ignore";
            // assert
            Assert.That(sut.ErrorMessage, Is.EqualTo("Username cannot be empty\n"));
        }

        [Test]
        public void T006_After_Login_Command_Was_Executed_Busy_Indicator_Should_Be_Triggered_Using_IsBusy_Property()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            // act
            sut.Username = "ignore";
            sut.Password = "ignore";
            sut.LoginCommand.Execute();
            // assert
            Assert.True(sut.IsBusy);
        }

        [Test]
        public void T007_When_No_User_Was_Found_Should_Display_Proper_Message_With_Ok_Button_Enabled()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            LoginViewModelTest.REPOSITORY_ACCESS_TIME_IN_MS = 0;

            // act
            sut.Username = "ignore";
            sut.Password = "ignore";
            sut.LoginCommand.Execute();

            while (sut.IsBusy)
            {
                Thread.Sleep(50);
            }

            // assert
            Assert.True(sut.LoginErrorOKCommand.CanExecute());
            Assert.That(sut.LoginErrorMessageVisibility, Is.EqualTo(Xceed.Wpf.Toolkit.WindowState.Open));
            Assert.That(sut.LoginErrorMessage, Is.EqualTo("User not found, username or password invalid"));
        }

        [Test]
        public void T008_When_User_Was_Found_Should_Send_A_Proper_Message_Using_EventAggregator()
        {
            // arrange
            var sut = this.container.Resolve<ILoginViewModel>();
            LoginViewModelTest.REPOSITORY_ACCESS_TIME_IN_MS = 0;
            LoginViewModelTest.REPOSITORY_SHOULD_FIND_USER = true;
            LoginViewModelTest.EVENTAGGREGATOR_USED = false;
            LoginViewModelTest.REPOSITORY_USER_HASHED_PASSWORD = "ignore";

            // act
            sut.Username = "ignore";
            sut.Password = "ignore";
            sut.LoginCommand.Execute();

            int attemps = 3;
            while (LoginViewModelTest.EVENTAGGREGATOR_USED==false && attemps>0)
            {
                Thread.Sleep(50);
                attemps--;
            }

            // assert
            Assert.True(LoginViewModelTest.EVENTAGGREGATOR_USED);
        }

    }

    public class LoginViewModelMockEventAggregator : IEventAggregator
    {
        public TEventType GetEvent<TEventType>() where TEventType : EventBase, new()
        {
            LoginViewModelTest.EVENTAGGREGATOR_USED = true;
            return new TEventType();
        }
    }

    public class LoginViewModelMockRepository : IRepository
    {
        public IEnumerable<Common.Model.ITradeListElemet> GetTradeList(int startIndex = 0, int count = int.MaxValue, TradeFilter filter = null)
        {
            throw new NotImplementedException();
        }

        public Common.Model.ITradeCapture GetTrade(int tradeId)
        {
            throw new NotImplementedException();
        }

        public void Save(Common.Model.ITradeCapture trade)
        {
            throw new NotImplementedException();
        }

        public int GetTradeCount(TradeFilter filter = null)
        {
            throw new NotImplementedException();
        }

        public Common.Model.ITradeUser LoadUserByName(string username)
        {
            Thread.Sleep(LoginViewModelTest.REPOSITORY_ACCESS_TIME_IN_MS);
            if (LoginViewModelTest.REPOSITORY_SHOULD_FIND_USER)
            {
                return new TradeUser
                {
                    HashPassword = LoginViewModelTest.REPOSITORY_USER_HASHED_PASSWORD
                };
            }
            return null;
        }
    }
}

