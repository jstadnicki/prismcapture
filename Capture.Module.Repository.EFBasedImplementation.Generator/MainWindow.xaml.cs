﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Capture.Model;
using Microsoft.Win32;
using System.Threading;
using Capture.Common.Model;
using System.IO;

namespace Capture.Module.Repository.EFBasedImplementation.Generator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            tabloadedactions.Add(0, this.RefreshCaptures);
            tabloadedactions.Add(1, this.UpdateAccontsInfortion); // users - no need for update layout
            tabloadedactions.Add(2, () => { }); // views - no need for update
            tabloadedactions.Add(3, this.RefreshHistories); // history - no need for update
            tabloadedactions.Add(4, () => { }); // queries - no need for update
            tabloadedactions.Add(5, this.UpdateStatusesInformation); // statuses
            tabloadedactions.Add(6, this.UpdateAccountsInformation); // accounts

            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        Dictionary<int, string> usersList = new Dictionary<int, string>();
        Dictionary<int, string> statuses = new Dictionary<int, string>();
        List<int> viewsList = new List<int>();
        List<int> trades = new List<int>();
        Dictionary<int, string> accountTypes = new Dictionary<int, string>();
        byte[] userimage;

        Dictionary<int, Action> tabloadedactions = new Dictionary<int, Action>();

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void UpdateAccontsInfortion()
        {
            this.accountTypes.Clear();
            using (var db = new TCEntities())
            {
                foreach (var a in db.Accounts.Where(x=>x.AccessLevel<5))
                {
                    this.accountTypes.Add(a.Id, a.Name);
                }
            }
            this.NewUserAccountType.ItemsSource = this.accountTypes.Select(a => a.Value);
            this.NewUserAccountType.SelectedIndex = 0;
        }

        private void AddCaptureClick(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                var newcapture = Trade.CreateTrade(-1,
                    this.usersList.ElementAt(this.users.SelectedIndex).Key,
                    datecapture.SelectedDate.Value,
                    this.viewsList.ElementAt(this.views.SelectedIndex));

                db.Trades.AddObject(newcapture);
                db.SaveChanges();
            }

            RandomizeCapture();
        }

        private List<int> GetViews()
        {
            using (var db = new TCEntities())
            {
                return db.ViewSchemas.Select(v => v.Id).ToList();
            }
        }

        private Dictionary<int, string> GetUsers()
        {
            var result = new Dictionary<int, string>();
            using (var db = new TCEntities())
            {
                foreach (var user in db.Users)
                {
                    result.Add(user.Id, user.Username);
                }
            }
            return result;
        }

        private void AddUserClick(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                var newuser = User.CreateUser(-1, 
                    this.username.Text, 
                    this.password.Text, 
                    this.isactive.IsChecked.Value,
                    this.accountTypes
                            .Where(t=>t.Value == (string)this.NewUserAccountType.SelectedValue)
                            .Single().Key,
                    "firstname", "lastname");
                newuser.Image = this.userimage;
                db.Users.AddObject(newuser);
                db.SaveChanges();
            }
        }

        private void RefreshHistories()
        {
            usersList = this.GetUsers();
            this.author.ItemsSource = usersList.Values;

            trades = this.GetTrades();
            this.tradeid.ItemsSource = trades;

            this.statuses = this.GetStatuses();
            this.status.ItemsSource = this.statuses.Values;


            RandomizeHistory();
        }

        private Dictionary<int, string> GetStatuses()
        {
            var result = new Dictionary<int, string>();
            using (var db = new TCEntities())
            {
                foreach (var s in db.Statuses)
                {
                    result.Add(s.Id, s.StatusName);
                }
            }
            return result;
        }

        private List<int> GetTrades()
        {
            using (var db = new TCEntities())
            {
                return db.Trades.Select(t => t.Id).ToList();
            }
        }

        private void RefreshCaptures()
        {
            usersList = this.GetUsers();
            this.users.ItemsSource = usersList.Values;

            viewsList = this.GetViews();
            this.views.ItemsSource = viewsList;

            datecapture.SelectedDate = DateTime.Now;
            RandomizeCapture();
        }

        void RandomizeCapture()
        {
            Random r = new Random(DateTime.Now.Millisecond);

            this.users.SelectedIndex = r.Next(usersList.Count());
            this.views.SelectedIndex = r.Next(viewsList.Count());
        }

        private void AddViewClick(object sender, RoutedEventArgs e)
        {
            var newview = ViewSchema.CreateViewSchema(-1, this.schema.Text);
            using (var db = new TCEntities())
            {
                db.ViewSchemas.AddObject(newview);
                db.SaveChanges();
            }
        }

        private void BrowseClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                using (var instream = System.IO.File.OpenText(ofd.FileName))
                {
                    this.schema.Text = instream.ReadToEnd();
                }
            }
        }

        private void TabControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source == this.tabcontrol)
            {
                this.tabloadedactions[this.tabcontrol.SelectedIndex]();
            }
        }

        private void ShowUsers_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.Users.Select(u => new { u.Id, u.FirstName, u.LastName, u.Account1.Name, u.IsActive, u.Username, u.Userpassword });
            }
        }

        private void ShowTrades_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.Trades.ToList();
            }
        }

        private void ShowHistories_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.Histories.ToList();
            }
        }

        private void ShowViews_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.ViewSchemas.ToList();
            }
        }

        private void ShowStatuses_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.Statuses.ToList();
            }
        }

        private void makehistory_Click(object sender, RoutedEventArgs e)
        {
            Random r = new Random(DateTime.Now.Millisecond);

            var authorid = usersList.ElementAt(this.author.SelectedIndex);
            var created = this.created.SelectedDate.Value;
            created = created.Subtract(new TimeSpan(r.Next()));
            var tradeid = (int)this.tradeid.SelectedValue;
            var status = statuses.ElementAt(this.status.SelectedIndex);

            var nh = History.CreateHistory(-1, authorid.Key, created, tradeid, status.Key);
            nh.Annualization = this.annualization.Value;
            nh.AnnualizationEnabled = this.annualizationenabled.IsChecked;
            //nh.bit = this.
            //nh.CalculatorDelta = this.calcu
            //nh.CalculatorFWD=this.calcu
            //nh.CalculatorIRCorrection = this.cal
            //nh.CalculatorMaxString
            //nh.CalculatorMinStrike
            //nh.CalculatorName
            //nh.CalculatorStrike
            //nh.CalculatorStrikeUsed
            //nh.CalculatorVolOfVol
            //nh.CalculatorVolSpread
            nh.Composite = this.composite.IsChecked;
            nh.Contract = this.contract.Text;
            //nh.DetailsAgent = this.deta
            //nh.DetailsBook = this.det
            //nh.DetailsBrokerCode
            //nh.DetailsCounterBook
            //nh.DetailsCptyCode
            //nh.DetailsMarketer
            //nh.DetailsTrader
            //nh.EconomicComments = this.econo
            nh.EffectiveDate = this.effectivedate.SelectedDate;
            nh.ExpectedN = this.expectedn.Value;
            nh.ExpectedNEnabled = this.exprectednenabled.IsChecked;
            //nh.Expiry = this.expi;
            nh.ExpiryDataCallendar = this.expirydatecallendar.Text;
            nh.ExpiryDate = this.expirydate.SelectedDate;
            nh.ExpiryOffset = new DateTimeOffset(DateTime.Now);// missing offset
            nh.FixFinalParamEnabled = this.fixlastparam.IsChecked;
            nh.FixFinalSetting = this.fixlastsetting.Value;
            nh.FixFirstParamEnabled = this.fixfirstparam.IsChecked;
            nh.FixFirstSetting = this.fixfirstsetting.Value;
            //nh.Frequency = this.fre
            nh.IndexName = this.indexname.Text;
            //nh.InitialiExchange = this.initia
            //nh.ManageRisk = this.mana
            //nh.MultipleExchange=this.mul
            nh.NationalCurrency = this.nationalcurrency.Text;
            nh.Nominal = this.nominalcapture.Value;
            //nh.NominalMarketDisrupt=this.n
            nh.NonDeliverable = this.nondeliverable.Text;
            nh.NonDeliverableEnabled = this.nondeliveryenabled.IsChecked;
            nh.ObsoleteCondition = this.obsoletecondition.Text;
            nh.PaymentCallendar = this.paymentcallendar.Text;
            nh.PaymentCurrency = this.paymentcurrency.Text;
            nh.PaymentDate = this.paymentdate.SelectedDate;
            nh.PaymentLag = this.paymentlag.Value;
            //nh.Periods = this.per
            nh.ProductType = this.producttype.Text;
            nh.ReferenceVolatility = this.referencevolatility.Value;
            //nh.RegularDatesFirst = this.reg
            //nh.RegulatDatesLast = this.reg
            //nh.RollDay = this.roll
            //nh.RollingMCD = this.ro
            nh.Selling = this.selling.IsChecked;
            nh.Strike = this.strike.Value;
            nh.StructureType = this.structuretype.Text;
            nh.SubstractDays = new DateTimeOffset(DateTime.Now);//, TimeSpan.FromDays(this.expiryoffset.Value.Value));
            nh.SwapType = this.swaptype.Text;
            nh.Units = this.units.Value;
            nh.VegaAmount = this.vegaamount.Value;
            nh.VolumeCapture = this.volumecapture.Value;
            nh.VolumeCaptureEnabled = this.volumecaptureenable.IsChecked;

            using (var db = new TCEntities())
            {
                db.Histories.AddObject(nh);
                db.SaveChanges();
            }

            this.RefreshHistories();
        }

        private void RandomizeHistory()
        {
            Random r = new Random(DateTime.Now.Millisecond);
            this.author.SelectedIndex = r.Next(usersList.Count);
            this.status.SelectedIndex = r.Next(statuses.Count);

            //this.trades = this.GetTrades();

            string[] contracts = { "Nokia", "MS", "Linux", "ATI", "Food", "Drink", "Sleep", "Time" };
            string[] indexes = { "ASD", "SDF", "DFG", "FGH", "GHJ", "HJK", "JKL", "KL2", "ZXC", "XCV", "CVB", "VBN", "BNM", "NM3", "343", };
            string[] currencies = { "PLN", "USD", "CAN", "BLV", "JPN", "EUR", "KOZ" };
            string[] producttype = { "p1", "p12", "prod1", "prod2", "prod3", "prod4", "prod5", "prod9" };

            this.tradeid.SelectedIndex = r.Next(this.trades.Count);
            var createdDate = DateTime.Now.AddDays(r.Next(-2000, 0));
            this.created.SelectedDate = createdDate;

            this.annualization.Value = r.Next(50, 100);
            this.annualizationenabled.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);
            this.composite.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);

            this.contract.Text = contracts[r.Next(contracts.Length)];
            this.effectivedate.SelectedDate = DateTime.Now.AddDays(r.Next(-2000, 0));
            this.expectedn.Value = r.Next(50, 100);
            this.exprectednenabled.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);


            this.expirydatecallendar.Text = (DateTime.Now.Millisecond % 2 == 0).ToString();
            //Thread.Sleep(3);

            this.expirydate.SelectedDate = DateTime.Now.AddDays(r.Next(-2000, 0));

            this.expiryoffset.Value = r.Next(50, 100);
            this.fixlastparam.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);

            this.fixlastsetting.Value = r.Next(50, 100);
            this.fixfirstparam.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);

            this.fixfirstsetting.Value = r.Next(50, 100);

            this.indexname.Text = indexes[r.Next(indexes.Length)];
            this.nationalcurrency.Text = currencies[r.Next(currencies.Length)];
            this.nominalcapture.Value = r.Next(50, 100);

            this.nondeliverable.Text = (DateTime.Now.Millisecond % 2 == 0).ToString();
            //Thread.Sleep(3);

            this.nondeliveryenabled.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);


            this.obsoletecondition.Text = (DateTime.Now.Millisecond % 2 == 0).ToString();
            //Thread.Sleep(3);

            this.paymentcallendar.Text = currencies[r.Next(currencies.Length)];
            this.paymentcurrency.Text = currencies[r.Next(currencies.Length)];
            this.paymentdate.SelectedDate = DateTime.Now.AddDays(r.Next(-2000, 0));
            this.paymentlag.Value = r.Next(50, 100);

            this.producttype.Text = producttype[r.Next(producttype.Length)];
            this.referencevolatility.Value = r.Next(50, 100);
            this.selling.IsChecked = DateTime.Now.Millisecond % 2 == 0;
            //Thread.Sleep(3);

            this.strike.Value = r.Next(50, 100);

            this.structuretype.Text = DateTime.Now.Millisecond % 2 == 0 ? "hard" : "soft";
            //Thread.Sleep(3);

            this.expiryoffset.Value = r.Next(50, 100);
            this.swaptype.Text = DateTime.Now.Millisecond % 2 == 0 ? "buy" : "sell";
            //Thread.Sleep(3);

            this.units.Value = r.Next(50, 100);
            this.vegaamount.Value = r.Next(50, 100);
            this.volumecapture.Value = r.Next(50, 100);
            this.volumecaptureenable.IsChecked = DateTime.Now.Millisecond % 2 == 0;
        }

        private void nCapturesToAdd_Click(object sender, RoutedEventArgs e)
        {
            int c = int.Parse(this.capturesToAdd.Text);
            for (int i = 0; i < c; i++)
            {
                this.AddCaptureClick(null, null);
            }
        }

        private void nHistoriesToAdd_Click(object sender, RoutedEventArgs e)
        {
            int c = int.Parse(this.historiesToAdd.Text);
            for (int i = 0; i < c; i++)
            {
                this.makehistory_Click(null, null);
            }
        }

        private void UpdateAccountsInformation()
        {
            using (var db = new TCEntities())
            {
                if (db.Accounts.Count() > 0)
                {
                    var j = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Junior).FirstOrDefault();
                    var r = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Regular).FirstOrDefault();
                    var s = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Senior).FirstOrDefault();
                    var m = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Manager).FirstOrDefault();

                    if (j != null)
                        this.JuniorAccountName.Text = j.Name;
                    if (r != null)
                        this.RegularAccountName.Text = r.Name;
                    if (s != null)
                        this.SeniorAccountName.Text = s.Name;
                    if (m != null)
                        this.ManagerAccountName.Text = m.Name;
                }
            }
        }

        private void AccountSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                if (db.Accounts.Count() > 0)
                {
                    UpdateAccountNames(db);
                }
                else
                {
                    CreateAccounts(db);
                }

                db.SaveChanges();
            }
        }

        private void CreateAccounts(TCEntities db)
        {
            var admin = Account.CreateAccount(0, "Administrator", (int)AccountType.Administrator);

            var junior = Account.CreateAccount(1, this.JuniorAccountName.Text, (int)AccountType.Junior);
            var regular = Account.CreateAccount(2, this.RegularAccountName.Text, (int)AccountType.Regular);
            var senior = Account.CreateAccount(3, this.SeniorAccountName.Text, (int)AccountType.Senior);
            var manager = Account.CreateAccount(4, this.ManagerAccountName.Text, (int)AccountType.Manager);

            db.Accounts.AddObject(admin);
            db.Accounts.AddObject(junior);
            db.Accounts.AddObject(regular);
            db.Accounts.AddObject(senior);
            db.Accounts.AddObject(manager);
        }

        private void UpdateAccountNames(TCEntities db)
        {
            var j = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Junior).FirstOrDefault();
            var r = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Regular).FirstOrDefault();
            var s = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Senior).FirstOrDefault();
            var m = db.Accounts.Where(a => a.AccessLevel == (int)AccountType.Manager).FirstOrDefault();

            if (j != null)
                j.Name = this.JuniorAccountName.Text;
            if (r != null)
                r.Name = this.RegularAccountName.Text;
            if (s != null)
                s.Name = this.SeniorAccountName.Text;
            if (m != null)
                m.Name = this.ManagerAccountName.Text;
        }

        private void ShowAccounts_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                this.querygridresult.ItemsSource = db.Accounts.ToList();
            }
        }

        private void BrowseForImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                using( var file = File.Open(ofd.FileName, FileMode.Open))
                {
                    this.userimage = new byte[(int)file.Length];
                    file.Read(this.userimage, 0, (int)file.Length);
                }
                BitmapImage bi = new BitmapImage(new Uri(ofd.FileName, UriKind.Absolute));               
                this.userpicture.Source = bi;
                bi = null;
            }
        }

        private void StatusSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new TCEntities())
            {
                if (db.Statuses.Count() > 0)
                {
                    this.UpdateStatusesNames(db);
                }
                else
                {
                    this.CreateStatuses(db);
                }

                db.SaveChanges();
            }
        }

        private void CreateStatuses(TCEntities db)
        {
            var newtrade = Status.CreateStatus(-1, this.NewTradeName.Text);
            var underreview = Status.CreateStatus(-1, this.UnderReviewName.Text);
            var proposed = Status.CreateStatus(-1, this.ProposedName.Text);
            var accepted = Status.CreateStatus(-1, this.AcceptedName.Text);
            var rejected = Status.CreateStatus(-1, this.RejectedName.Text);
            var deleted = Status.CreateStatus(-1, this.DeletedName.Text);
            var executed = Status.CreateStatus(-1, this.ExecutedName.Text);

            db.Statuses.AddObject(newtrade);
            db.Statuses.AddObject(underreview);
            db.Statuses.AddObject(proposed);
            db.Statuses.AddObject(accepted);
            db.Statuses.AddObject(rejected);
            db.Statuses.AddObject(deleted);
            db.Statuses.AddObject(executed);
        }

        private void UpdateStatusesNames(TCEntities db)
        {
            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.NewTrade)
                .First().StatusName = this.NewTradeName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.UnderReview)
                .First().StatusName = this.UnderReviewName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.Proposed)
                .First().StatusName = this.ProposedName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.Accepted)
                .First().StatusName = this.AcceptedName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.Rejected)
                .First().StatusName = this.RejectedName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.Deleted)
                .First().StatusName = this.DeletedName.Text;

            db.Statuses
                .Where(s => s.Id == (int)Common.Model.TradeStatus.Executed)
                .First().StatusName = this.ExecutedName.Text;
        }

        private void UpdateStatusesInformation()
        {
            using (var db = new TCEntities())
            {
                if (db.Statuses.Count() > 0)
                {
                    this.NewTradeName.Text = db.Statuses
                    .Where(s => s.Id == (int)Common.Model.TradeStatus.NewTrade)
                    .First().StatusName;

                    this.UnderReviewName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.UnderReview)
                        .First().StatusName;

                    this.ProposedName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.Proposed)
                        .First().StatusName;

                    this.AcceptedName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.Accepted)
                        .First().StatusName;

                    this.RejectedName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.Rejected)
                        .First().StatusName;

                    this.DeletedName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.Deleted)
                        .First().StatusName;

                    this.ExecutedName.Text = db.Statuses
                        .Where(s => s.Id == (int)Common.Model.TradeStatus.Executed)
                        .First().StatusName;
                }
            }
        }
       
    }
}
