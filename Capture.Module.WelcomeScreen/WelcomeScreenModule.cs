﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Prism.Regions;
using Capture.Module.WelcomeScreen.MVVM;
using Capture.Common;

namespace Capture.Module.WelcomeScreen
{
    public class WelcomeScreenModule : IModule
    {
        private IUnityContainer container;
        private IRegionManager regionManager;
        
        public WelcomeScreenModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.container.RegisterType<IWelcomeScreenViewModel, WelcomeScreenViewModel>();
            this.container.RegisterType<object, WelcomeScreenView>(typeof(IWelcomeScreenView).FullName);

            this.RegisterViews();
        }

        private void RegisterViews()
        {
            this.regionManager.RegisterViewWithRegion(RegionNames.CenterFillRegion, typeof(WelcomeScreenView));
        }
    }
}
