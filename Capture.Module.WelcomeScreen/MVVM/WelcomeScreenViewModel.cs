﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.MVVM;
using System.Threading.Tasks;
using System.Threading;
using Capture.Common.Model;
using Capture.Common.Repository;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Regions;
using Capture.Common.Prism;
using Capture.Common.Messages;

namespace Capture.Module.WelcomeScreen.MVVM
{
    public class WelcomeScreenViewModel : ViewModelBase, IWelcomeScreenViewModel
    {

        public WelcomeScreenViewModel(ITradeUser currentUser, IRepository repository, IEventAggregator eventAggregator)
        {
            this.currentUser = currentUser;
            this.eventAggregator = eventAggregator;
            this.repository = repository;
            this.StartCurrentTimeWorker();
            SelectedTabControlIndex = 0;
            this.GetCurrentUserDocumentsCountInformationAsync();
            LoadSelectedRecentDocumentCommand = new DelegateCommand(this.LoadSelectedRecentDocumentExecute, this.LoadSelectedRecentDocumentCanExecute);
        }

        private void GetCurrentUserDocumentsCountInformationAsync()
        {
            ThreadPool.QueueUserWorkItem(_ =>
                {
                    this.AcceptedDocumtnsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.Accepted);
                    this.ExecutedDocumentsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.Executed);
                    this.NewDocumentsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.NewTrade);
                    this.ProposedDocumentsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.Proposed);
                    this.RejectedDocumentsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.Rejected);
                    this.UnderReviewDocumentsCount = this.CountMyDocumentBasedOnStatus(TradeStatus.UnderReview);
                }, null);
        }

        private void LoadSelectedRecentDocumentExecute()
        {
            this.eventAggregator
                .GetEvent<TradeCaptureApplicationCommands>()
                .Publish(new TradeApplicationCommand
                {
                    LoadTradeId = this.LastModifiedDocumentSelected.TradeId,
                    CommandType = ApplicationCommandsType.LoadTradeWithId
                });
        }

        private bool LoadSelectedRecentDocumentCanExecute()
        {
            return this.LastModifiedDocumentSelected != null;
        }

        private IEnumerable<ITradeListElemet> LoadRecentlyModifiedTrades(int p)
        {
            var filter = new TradeFilter()
                    .AddAuthorSpecification(this.currentUser.Id)
                    .AddLastModificationAfter(DateTime.Now.AddDays(p * -1));

            return this.repository.GetTradeList(0, int.MaxValue, filter);
        }

        int CountMyDocumentBasedOnStatus(TradeStatus status)
        {
            var myNewDocumentsFilter = new TradeFilter()
                        .AddAuthorSpecification(this.UserId)
                        .AddFilterSpecification(status);

            return this.repository.GetTradeCount(myNewDocumentsFilter);
        }

        private void StartCurrentTimeWorker()
        {
            this.CurrentTime = DateTime.Now.ToLongTimeString();
            this.timerUpdater = Task.Factory.StartNew(vm =>
            {
                while (1 == 1)
                {
                    Thread.Sleep(1000);
                    (vm as WelcomeScreenViewModel).CurrentTime = DateTime.Now.ToLongTimeString();
                }
            }, this);
        }

        public string CurrentFullDayName
        {
            get
            {
                return DateTime.Now.DayOfWeek.ToString();
            }
        }

        public string CurrentDate
        {
            get
            {
                return DateTime.Now.ToLongDateString();
            }
        }

        string currentTime;
        private Task timerUpdater;
        private ITradeUser currentUser;
        private IRepository repository;
        private int newDocumentsCount;
        private int underReviewDocumentsCount;
        private int rejectedDocumentsCount;
        private int proposedDocumentsCount;
        private int executedDocumentsCount;
        private int acceptedDocumtnsCount;
        private ITradeListElemet lastModifiedDocumentSelected;
        private IEnumerable<ITradeListElemet> lastModifiedDocuments;
        private int selectedTabControlIndex;
        private bool recentModificationIsBusy;

        Dictionary<int, int> selectedTabToDaysMapping = new Dictionary<int, int>
        {
            {0,7},
            {1,30},
            {2,90}
        };
        private IRegionManager regionManager;
        private IEventAggregator eventAggregator;

        public string CurrentTime
        {
            get
            {
                return this.currentTime;
            }
            set
            {
                this.currentTime = value;
                OnPropertyChanged("CurrentTime");
            }

        }

        public byte[] UserImage
        {
            get { return this.currentUser.UserPicture; }
        }

        public string UserFullName
        {
            get
            {
                return this.currentUser.FullName;
            }
        }

        public int UserId
        {
            get
            {
                return this.currentUser.Id;
            }
        }

        public ITradeListElemet LastModifiedDocumentSelected
        {
            get
            {
                return this.lastModifiedDocumentSelected;
            }
            set
            {
                this.lastModifiedDocumentSelected = value;
                OnPropertyChanged("LastModifiedDocumentSelected");
                this.LoadSelectedRecentDocumentCommand.RaiseCanExecuteChanged();
            }
        }

        public IEnumerable<ITradeListElemet> LastModifiedDocuments
        {
            get
            {
                return this.lastModifiedDocuments;
            }
            private set
            {
                this.lastModifiedDocuments = value;
                OnPropertyChanged("LastModifiedDocuments");
            }
        }


        public int NewDocumentsCount
        {
            get
            {
                return this.newDocumentsCount;
            }
            set
            {
                this.newDocumentsCount = value;
                OnPropertyChanged("NewDocumentsCount");
            }
        }


        public int AcceptedDocumtnsCount
        {
            get
            {
                return this.acceptedDocumtnsCount;
            }
            set
            {
                this.acceptedDocumtnsCount = value;
                OnPropertyChanged("AcceptedDocumtnsCount");
            }
        }

        public int ExecutedDocumentsCount
        {
            get
            {
                return this.executedDocumentsCount;
            }
            set
            {
                this.executedDocumentsCount = value;
                OnPropertyChanged("ExecutedDocumentsCount");
            }
        }

        public int ProposedDocumentsCount
        {
            get
            {
                return this.proposedDocumentsCount;
            }
            set
            {
                this.proposedDocumentsCount = value;
                OnPropertyChanged("ProposedDocumentsCount");
            }
        }

        public int RejectedDocumentsCount
        {
            get
            {
                return this.rejectedDocumentsCount;
            }
            set
            {
                this.rejectedDocumentsCount = value;
                OnPropertyChanged("RejectedDocumentsCount");
            }
        }

        public int UnderReviewDocumentsCount
        {
            get
            {
                return this.underReviewDocumentsCount;
            }
            set
            {
                this.underReviewDocumentsCount = value;
                OnPropertyChanged("UnderReviewDocumentsCount");
            }
        }


        public int SelectedTabControlIndex
        {
            get
            {
                return this.selectedTabControlIndex;
            }
            set
            {
                RecentModificationIsBusy = true;
                this.selectedTabControlIndex = value;
                OnPropertyChanged("SelectedTabControlIndex");
                TriggerSearchForRecentModifications();
            }
        }

        private void TriggerSearchForRecentModifications()
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                this.LastModifiedDocuments = this.LoadRecentlyModifiedTrades(this.selectedTabToDaysMapping[this.SelectedTabControlIndex]);
                RecentModificationIsBusy = false;
            }, null);
        }

        public bool RecentModificationIsBusy
        {
            get
            {
                return this.recentModificationIsBusy;
            }
            set
            {
                this.recentModificationIsBusy = value;
                OnPropertyChanged("RecentModificationIsBusy");
            }
        }


        public DelegateCommand LoadSelectedRecentDocumentCommand
        {
            get;
            private set;
        }
    }
}
