﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Capture.Common.Model;
using Microsoft.Practices.Prism.Commands;

namespace Capture.Module.WelcomeScreen.MVVM
{
    public interface IWelcomeScreenViewModel
    {
        string CurrentFullDayName { get; }
        string CurrentDate { get; }
        string CurrentTime { get; }
        byte[] UserImage { get; }
        string UserFullName { get; }
        int UserId { get; }

        int NewDocumentsCount { get; }
        int AcceptedDocumtnsCount { get; }
        int ExecutedDocumentsCount { get; }
        int ProposedDocumentsCount { get; }
        int RejectedDocumentsCount { get; }
        int UnderReviewDocumentsCount { get; }

        int SelectedTabControlIndex { get; set; }

        bool RecentModificationIsBusy { get; }

        DelegateCommand LoadSelectedRecentDocumentCommand { get; }

        ITradeListElemet LastModifiedDocumentSelected { get; set; }
        IEnumerable<ITradeListElemet> LastModifiedDocuments { get; }
    }
}
